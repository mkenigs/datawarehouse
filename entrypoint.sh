#! /bin/bash
export CELERY_FILESYSTEM_PATH="${CELERY_FILESYSTEM_PATH:-$(mktemp -d -t dw-celery.XXXXX)}"
export LOGS_PATH="${LOGS_PATH:-$(mktemp -d -t dw-logs.XXXXX)}"

python3 manage.py migrate
# canonical is_true check, see pipeline-definition
if [[ "${IS_PRODUCTION:-}" = [Tt]rue ]] ; then
    python3 manage.py collectstatic
    supervisord
    tail --follow=name --retry /logs/nginx-{access,error}.log
else
    supervisord -c /code/configs/supervisor-app.devel.ini
fi
