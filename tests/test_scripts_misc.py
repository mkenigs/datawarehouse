"""Test the scripts.misc module."""
import datetime
from unittest import mock

import django.contrib.auth.models as auth_models
from django.utils import timezone
from freezegun import freeze_time

from datawarehouse import models
from datawarehouse.api.kcidb import serializers
from datawarehouse.scripts import misc
from tests import utils


class ScriptsMiscTest(utils.TestCase):
    """Unit tests for the scripts.misc module."""

    @staticmethod
    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signal_receivers.utils.send_kcidb_notification')
    def test_send_kcidb_objects_for_retriage(send_notif):
        """Test send_kcidb_object_for_retriage."""
        def get_date(days_ago):
            """Get timestamp from {days_ago}."""
            return timezone.now() - datetime.timedelta(days=days_ago)

        origin = models.KCIDBOrigin.objects.create(name='redhat')

        revision_1 = models.KCIDBRevision.objects.create(
            origin=origin, id='rev1',
            discovery_time=get_date(2),
            valid=False
        )
        models.KCIDBRevision.objects.create(
            origin=origin, id='rev2',
            discovery_time=get_date(2),
            valid=True,
        )
        models.KCIDBRevision.objects.create(
            origin=origin, id='rev3',
            discovery_time=get_date(4),
            valid=False,
        )
        build_1 = models.KCIDBBuild.objects.create(
            origin=origin, revision=revision_1, id='redhat:build-1',
            valid=False, start_time=get_date(2)
        )
        models.KCIDBBuild.objects.create(
            origin=origin, revision=revision_1, id='redhat:build-2',
            valid=True, start_time=get_date(2)
        )
        models.KCIDBBuild.objects.create(
            origin=origin, revision=revision_1, id='redhat:build-3',
            valid=False, start_time=get_date(4)
        )
        test_1 = models.KCIDBTest.objects.create(
            origin=origin, build=build_1, id='redhat:test-1',
            status=models.ResultEnum.FAIL,
            start_time=get_date(2)
        )
        models.KCIDBTest.objects.create(
            origin=origin, build=build_1, id='redhat:test-2',
            status=models.ResultEnum.PASS,
            start_time=get_date(2)
        )
        models.KCIDBTest.objects.create(
            origin=origin, build=build_1, id='redhat:test-3',
            status=models.ResultEnum.FAIL,
            start_time=get_date(4)
        )

        misc.send_kcidb_object_for_retriage(3)
        send_notif.assert_has_calls(
            [
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'revision',
                    'object': serializers.KCIDBRevisionSerializer(revision_1).data,
                    'id': revision_1.id,
                    'iid': revision_1.iid,
                }]),
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'build',
                    'object': serializers.KCIDBBuildSerializer(build_1).data,
                    'id': build_1.id,
                    'iid': build_1.iid,
                }]),
                mock.call([{
                    'timestamp': '2010-01-02T09:00:00+00:00',
                    'status': 'needs_triage',
                    'object_type': 'test',
                    'object': serializers.KCIDBTestSerializer(test_1).data,
                    'id': test_1.id,
                    'iid': test_1.iid,
                }]),
            ]
        )


class TestRegressionNotifications(utils.TestCase):
    """Unit tests for issue_regession methods."""

    @staticmethod
    def _populate(data):
        """Populate test data."""
        auth_models.Group.objects.bulk_create([
            auth_models.Group(name=group_name)
            for group_name in data.get('groups', [])
        ])

        models.Policy.objects.bulk_create([
            models.Policy(
                name=policy_name,
                read_group=auth_models.Group.objects.get(name=group_names['read']) if group_names['read'] else None,
                write_group=auth_models.Group.objects.get(name=group_names['write']) if group_names['write'] else None,
            ) for policy_name, group_names in data.get('policies', {}).items()
        ])

        models.GitTree.objects.bulk_create([
            models.GitTree(
                name=gittree_name,
                policy=(
                    models.Policy.objects.get(name=policy_name)
                    if policy_name else None
                )
            ) for gittree_name, policy_name in data.get('gittrees', {}).items()
        ])

        models.Issue.objects.bulk_create([
            models.Issue(
                kind=(
                    models.IssueKind.objects
                    .get_or_create(description='mock', tag='mock-tag')[0]
                ),
                description=issue,
                ticket_url=f'https://{issue}',
                policy=(
                    models.Policy.objects.get(name=policy_name)
                    if policy_name else None
                )
            ) for issue, policy_name in data.get('issues', {}).items()
        ])

        models.KCIDBRevision.objects.bulk_create([
            models.KCIDBRevision(
                origin=(
                    models.KCIDBOrigin.objects
                    .get_or_create(name='mock')[0]
                ),
                id=revision_id,
                tree=models.GitTree.objects.get(name=gittree_name),
            ) for revision_id, gittree_name in data.get('revisions', {}).items()
        ])

        for username, user_data in data.get('users', {}).items():
            user = auth_models.User.objects.create(
                username=username,
            )
            for group_name in user_data['groups']:
                group = auth_models.Group.objects.get(name=group_name)
                user.groups.add(group)
            for permission_codename in user_data['permissions']:
                permission = auth_models.Permission.objects.get(codename=permission_codename)
                user.user_permissions.add(permission)

    @mock.patch('datawarehouse.utils.async_send_email.delay')
    def test_notify_issue_regression(self, mock_send_mail):
        """Test notify_issue_regression method."""
        data = {
            'groups': ['group_1', 'group_2', 'another_group'],
            'policies': {
                'issue_policy': {'read': 'another_group', 'write': 'group_1'},
                'object_policy': {'read': 'group_2', 'write': 'another_group'},
            },
            'gittrees': {
                'gittree_1': 'object_policy',
            },
            'issues': {
                'issue_1': 'issue_policy',
            },
            'revisions': {
                'revision_1': 'gittree_1',
            },
            'users': {
                # These don't check all the boxes
                'user_1': {'groups': [], 'permissions': []},
                'user_2': {'groups': ['group_1'], 'permissions': []},
                'user_3': {'groups': ['group_2'], 'permissions': []},
                'user_4': {'groups': ['group_1', 'group_2'], 'permissions': []},
                'user_5': {'groups': [], 'permissions': ['change_issue']},
                'user_6': {'groups': ['group_1'], 'permissions': ['change_issue']},
                'user_7': {'groups': ['group_2'], 'permissions': ['change_issue']},
                # These ones are correct
                'user_8': {'groups': ['group_1', 'group_2'], 'permissions': ['change_issue']},
                'user_9': {'groups': ['group_1', 'group_2', 'another_group'],
                           'permissions': ['change_issue']},
            },
        }
        self._populate(data)

        revision = models.KCIDBRevision.objects.get(id='revision_1')
        issue = models.Issue.objects.get(description='issue_1')

        misc.notify_issue_regression(revision, issue)

        # We should've notified only two users
        self.assertTrue(mock_send_mail.called)
        self.assertEqual(mock_send_mail.call_count, 2)

        # Check we sent it to the correct ones
        users = auth_models.User.objects.filter(username__in=('user_8', 'user_9'))
        subject = f'{ issue.kind.tag } | Issue regression detected'
        message = (
            'Hello,\n\n'
            'You are receiving this email because a regression was detected\n'
            'and tagged in DataWarehouse.\n\n'
            f'Issue: { issue.description}\n'
            f'URL: { issue.web_url }\n'
            f'Seen in: { revision.web_url }\n\n'
            '--\n'
            'The DataWarehouse team.\n'
        )

        mock_send_mail.assert_has_calls(
            [mock.call(user_id=user.id, subject=subject, message=message) for user in users]
        )

    @mock.patch('datawarehouse.scripts.misc.notify_issue_regression')
    @mock.patch('datawarehouse.scripts.misc.settings.FF_NOTIFY_ISSUE_REGRESSION', True)
    def test_verify_issue_regression(self, mock_notify):
        """Test verify_issue_regression method."""
        data = {
            'gittrees': {
                'gittree_1': None,
            },
            'issues': {
                'issue_1': None,
            },
            'revisions': {
                'revision_1': 'gittree_1',
            },
        }
        self._populate(data)

        revision = models.KCIDBRevision.objects.get(id='revision_1')
        issue = models.Issue.objects.get(description='issue_1')

        # Issue not resolved, not a regression
        misc.verify_issue_regression(revision, issue)
        self.assertFalse(mock_notify.called)

        # Issue resolved, but already tagged in the revision, not a regression
        issue.resolved = True
        issue.save()
        revision.issues.add(issue)
        misc.verify_issue_regression(revision, issue)
        self.assertFalse(mock_notify.called)

        # Issue resolved, not tagged in the revision, is a regression
        revision.issues.remove(issue)
        misc.verify_issue_regression(revision, issue)
        self.assertTrue(mock_notify.called)

    @mock.patch('datawarehouse.scripts.misc.notify_issue_regression')
    @mock.patch('datawarehouse.scripts.misc.settings.FF_NOTIFY_ISSUE_REGRESSION', False)
    def test_verify_issue_regression_disable(self, mock_notify):
        """Test verify_issue_regression method. FF_NOTIFY_ISSUE_REGRESSION is disabled."""
        data = {
            'gittrees': {
                'gittree_1': None,
            },
            'issues': {
                'issue_1': None,
            },
            'revisions': {
                'revision_1': 'gittree_1',
            },
        }
        self._populate(data)

        revision = models.KCIDBRevision.objects.get(id='revision_1')
        issue = models.Issue.objects.get(description='issue_1')

        # Issue resolved, not tagged in the revision, is a regression, but it's disabled.
        misc.verify_issue_regression(revision, issue)
        self.assertFalse(mock_notify.called)
