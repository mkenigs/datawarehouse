"""Test authorization module."""
from unittest import mock

import django.contrib.auth.models as auth_models

from datawarehouse import authorization
from datawarehouse import models
from tests import utils


class TestPolicyAuthorizationBackend(utils.TestCase):
    """Test PolicyAuthorizationBackend authorization module."""

    @staticmethod
    def _populate(data):
        """Populate test data."""
        auth_models.Group.objects.bulk_create([
            auth_models.Group(name=group_name)
            for group_name in data.get('groups', [])
        ])

        models.Policy.objects.bulk_create([
            models.Policy(
                name=policy_name,
                read_group=auth_models.Group.objects.get(name=group_names['read']) if group_names['read'] else None,
                write_group=auth_models.Group.objects.get(name=group_names['write']) if group_names['write'] else None,
            ) for policy_name, group_names in data.get('policies', {}).items()
        ])

        models.GitTree.objects.bulk_create([
            models.GitTree(
                name=gittree_name,
                policy=(
                    models.Policy.objects.get(name=policy_name)
                    if policy_name else None
                )
            ) for gittree_name, policy_name in data.get('gittrees', {}).items()
        ])

        models.Issue.objects.bulk_create([
            models.Issue(
                kind=(
                    models.IssueKind.objects
                    .get_or_create(description='mock')[0]
                ),
                description=issue,
                ticket_url=f'https://{issue}',
                policy=(
                    models.Policy.objects.get(name=policy_name)
                    if policy_name else None
                )
            ) for issue, policy_name in data.get('issues', {}).items()
        ])

        for username, groups in data.get('users', {}).items():
            user = auth_models.User.objects.create(
                username=username,
            )
            for group_name in groups:
                group = auth_models.Group.objects.get(name=group_name)
                user.groups.add(group)

    def test_is_authorized_method(self):
        """
        Test _is_authorized.

        Check that _is_authorized looks for the correct key on the gittrees_authorization
        session data.

        Given 2 different gittrees and 2 authorizations, give the user a mix of them and
        check that it can read one and write the other.
        """
        data = {
            'groups': ['group_1', 'group_2', 'another_group'],
            'policies': {
                'policy-r1-wn': {'read': 'group_1', 'write': 'another_group'},
                'policy-rn-w2': {'read': 'another_group', 'write': 'group_2'},
            },
            'gittrees': {
                'gittree_1': 'policy-r1-wn',
                'gittree_2': 'policy-rn-w2'
            },
            'users': {},
        }
        self._populate(data)

        session = self.client.session
        session['user_groups'] = [g.id for g in auth_models.Group.objects.exclude(name='another_group')]
        session.save()

        cases = (
            ('gittree_1', 'read', True),
            ('gittree_1', 'write', False),
            ('gittree_2', 'read', False),
            ('gittree_2', 'write', True),
        )

        for gittree_name, method, authorized in cases:
            gittree = models.GitTree.objects.get(name=gittree_name)
            # pylint: disable=protected-access
            self.assertEqual(
                authorization.PolicyAuthorizationBackend._is_authorized(self.client, gittree, method),
                authorized,
                f'{gittree_name} - {method} - {authorized}',
            )

        # pylint: disable=protected-access
        # Ensure that an undefined method raises an exception.
        self.assertRaises(
            Exception,
            authorization.PolicyAuthorizationBackend._is_authorized, self.client, gittree, 'foobar'
        )

    @staticmethod
    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend._is_authorized')
    def test_is_read_authorized(is_authorized):
        """Test is_read_authorized calls _is_authorized correctly."""
        request = mock.Mock()
        gittree = mock.Mock()
        authorization.PolicyAuthorizationBackend.is_read_authorized(request, gittree)
        is_authorized.assert_called_with(request, gittree, 'read')

    @staticmethod
    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend._is_authorized')
    def test_is_write_authorized(is_authorized):
        """Test is_write_authorized calls _is_authorized correctly."""
        request = mock.Mock()
        gittree = mock.Mock()
        authorization.PolicyAuthorizationBackend.is_write_authorized(request, gittree)
        is_authorized.assert_called_with(request, gittree, 'write')

    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend.filter_authorized')
    def test_all_objects_authorized(self, filter_authorized):
        """Test all_objects_authorized."""
        queryset = mock.Mock()
        queryset.count.return_value = 1
        filter_authorized.return_value = queryset

        test_queryset = mock.Mock()

        # queryset.count != test_queryset.count
        test_queryset.count.return_value = 2
        self.assertFalse(
            authorization.PolicyAuthorizationBackend.all_objects_authorized(
                mock.Mock(), test_queryset
            )
        )

        # queryset.count == test_queryset.count
        test_queryset.count.return_value = 1
        self.assertTrue(
            authorization.PolicyAuthorizationBackend.all_objects_authorized(
                mock.Mock(), test_queryset
            )
        )

    def test_get_users_authorized(self):
        """Test get_users_authorized with read and write methods."""
        data = {
            'groups': ['group_read', 'group_write'],
            'policies': {
                'policy-private': {'read': 'group_read', 'write': 'group_write'},
                'policy-public': {'read': None, 'write': None},
            },
            'users': {
                'user_1': ['group_read'],
                'user_2': ['group_read', 'group_write'],
                'user_3': ['group_write'],
            },
            'issues': {
                'issue_1': 'policy-private',
                'issue_2': 'policy-public',
                'issue_3': None,
            }

        }
        self._populate(data)

        cases = (
            ('issue_1', {'read': ['user_1', 'user_2'],
                         'write': ['user_2', 'user_3']}),
            ('issue_2', {'read': ['user_1', 'user_2', 'user_3'],
                         'write': ['user_1', 'user_2', 'user_3']}),
            ('issue_3', {'read': [], 'write': []}),
        )

        for issue_description, users in cases:
            issue = models.Issue.objects.get(description=issue_description)
            for user in issue.users_read_authorized:
                self.assertIn(user.username, users['read'])
            for user in issue.users_write_authorized:
                self.assertIn(user.username, users['write'])

    def test_get_users_authorized_no_policy(self):
        """
        Test get_users_authorized with read and write methods.

        Model has no path_to_policy.
        """
        data = {
            'users': {
                'user_1': [],
                'user_2': [],
                'user_3': [],
            },
        }
        self._populate(data)

        test = models.Test.objects.create(name="foobar")

        self.assertListEqual(
            sorted([user.username for user in test.users_read_authorized]),
            sorted(['user_1', 'user_2', 'user_3']),
        )

        self.assertListEqual(
            sorted([user.username for user in test.users_write_authorized]),
            sorted(['user_1', 'user_2', 'user_3']),
        )


class TestRequestAuthorization(utils.TestCase):
    """Test RequestAuthorization middleware."""

    @mock.patch('datawarehouse.authorization.RequestAuthorization.fill_user_data')
    def test_call(self, fill_user_data):
        """
        Test __call__ calls to fill_user_data *before* calling get_response.

        Check that the request parameter for get_response call contains the
        modified data by fill_user_data.
        """
        def _fill_user_data_mock(request):
            request.session['dummy_data'] = {'foo': 'bar'}

        class GetResponseMock:
            # pylint: disable=too-few-public-methods
            """Mock get_response."""

            def __init__(self, testcase):
                """Save parent self to call assertEqual."""
                self.testcase = testcase

            def __call__(self, request):
                """Look for the dummy_data in the session."""
                self.testcase.assertEqual(
                    request.session['dummy_data'],
                    {'foo': 'bar'}
                )

        fill_user_data.side_effect = _fill_user_data_mock

        request = mock.Mock()
        request.session = {}

        authorization.RequestAuthorization(GetResponseMock(self))(request)
        self.assertTrue(fill_user_data.called)
        fill_user_data.assert_called_with(request)

    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend.get_user_groups')
    def test_fill_user_data_first_request(self, get_user_groups):
        """
        Test that fill_user_data updates the information on log in/out.

        First anonymous request updates user_id and calls get_user_groups.
        """
        request = mock.Mock()
        request.session = {}

        anonymous = auth_models.AnonymousUser()

        request.user = anonymous
        authorization.RequestAuthorization.fill_user_data(request)
        self.assertEqual('anonymous', request.session.get('user_id'))
        get_user_groups.assert_called_with(request)

    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend.get_user_groups')
    def test_fill_user_data_next_anon_request(self, get_user_groups):
        """
        Test that fill_user_data updates the information on log in/out.

        Following anonymous request don't call get_user_groups.
        """
        anonymous = auth_models.AnonymousUser()

        request = mock.Mock()
        request.user = anonymous
        request.session = {}
        request.session['user_id'] = 'anonymous'

        authorization.RequestAuthorization.fill_user_data(request)
        self.assertFalse(get_user_groups.called)

    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend.get_user_groups')
    def test_fill_user_data_login(self, get_user_groups):
        """
        Test that fill_user_data updates the information on log in/out.

        When AnonymousUser logs in, user_id is updated and get_user_groups is called.
        """
        request = mock.Mock()
        request.session = {}

        user = auth_models.User.objects.create(username='user_foo')

        request.user = user
        request.session['user_id'] = 'anonymous'

        authorization.RequestAuthorization.fill_user_data(request)
        self.assertEqual(user.id, request.session.get('user_id'))
        get_user_groups.assert_called_with(request)

    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend.get_user_groups')
    def test_fill_user_data_next_logged_in_request(self, get_user_groups):
        """
        Test that fill_user_data updates the information on log in/out.

        Following logged in request don't call get_user_groups.
        """
        user = auth_models.User.objects.create(username='user_foo')

        request = mock.Mock()
        request.user = user
        request.session = {}
        request.session['user_id'] = user.id

        authorization.RequestAuthorization.fill_user_data(request)
        self.assertFalse(get_user_groups.called)

    @mock.patch('datawarehouse.authorization.PolicyAuthorizationBackend.get_user_groups')
    def test_fill_user_data_logout(self, get_user_groups):
        """
        Test that fill_user_data updates the information on log in/out.

        User logs out and get_user_groups is called.
        """
        request = mock.Mock()
        request.session = {}

        anonymous = auth_models.AnonymousUser()
        user = auth_models.User.objects.create(username='user_foo')

        request.user = anonymous
        request.session['user_id'] = user.id

        authorization.RequestAuthorization.fill_user_data(request)
        self.assertEqual('anonymous', request.session.get('user_id'))
        get_user_groups.assert_called_with(request)
