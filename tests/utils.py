"""TestCase with utils."""
import json
import os

from django import test
import django.contrib.auth.models as auth_models
from django.contrib.auth.models import Permission  # pylint: disable=imported-auth-user
from django.contrib.auth.models import User
from django.core import serializers
from django.db.models import Q
from django.db.models.query import QuerySet
from django.test import Client

from datawarehouse import models

ASSETS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'assets')


class TestCase(test.TestCase):
    # pylint: disable=invalid-name
    """TestCase class."""

    def __init__(self, *args, **kwargs):
        """Override Init."""
        super(TestCase, self).__init__(*args, **kwargs)

    def _resolve_querysets(self, data):
        """Resolve QuerySets."""
        if isinstance(data, dict):
            for key, value in data.items():
                data[key] = self._resolve_querysets(value)
        elif isinstance(data, list):
            for key, value in enumerate(data):
                data[key] = self._resolve_querysets(value)
        elif isinstance(data, QuerySet):
            data = list(data)
        elif hasattr(data, 'object_list'):
            data = self._resolve_querysets(data.object_list)

        return data

    def assertContextEqual(self, first, second):
        """
        Assert Context items are equal.

        Second can contain only some of the keys from first.
        """
        for key in second.keys():
            self.assertEqual(self._resolve_querysets(first[key]), self._resolve_querysets(second[key]), key)

    @staticmethod
    def insertObjects(objects):
        """
        Insert objects into the database.

        Objects are formatted as returned by dumpdata --format json.
        """
        result = []
        for m in serializers.deserialize('json', json.dumps(list(objects))):
            m.save()
            result.append(m.object)
        return result

    @staticmethod
    def get_test_user():
        """Get a test user object."""
        try:
            return User.objects.get(username='test')
        except User.DoesNotExist:
            return User.objects.create_user(username='test')

    @staticmethod
    def get_authenticated_client(user):
        """Create and return an authenticated client instance."""
        client = Client()
        client.force_login(user=user)

        return client

    def assert_authenticated_post(self, response_code, permission_codename, *args, **kwargs):
        """Assert authenticated POST."""
        return self.assert_authenticated('post', response_code, permission_codename, *args, **kwargs)

    def assert_authenticated_delete(self, response_code, permission_codename, *args, **kwargs):
        """Assert authenticated DELETE."""
        return self.assert_authenticated('delete', response_code, permission_codename, *args, **kwargs)

    def assert_authenticated_put(self, response_code, permission_codename, *args, **kwargs):
        """Assert authenticated PUT."""
        return self.assert_authenticated('put', response_code, permission_codename, *args, **kwargs)

    def assert_authenticated_anonymous_forbidden(self, method, *args,
                                                 redirect_login=False, redirect_url=None, **kwargs):
        """Assert anonymous request is forbidden on an authenticated endpoint."""
        method_anon = getattr(self.client, method)
        response = method_anon(*args, **kwargs)
        if redirect_login:
            self.assertRedirects(response, redirect_url)
        else:
            self.assertIn(response.status_code, [400, 401, 403])
        return response

    def assert_authenticated(self, method, response_code, permission_codenames, *args,
                             user=None, redirect_login=False, redirect_url=None, **kwargs):
        """
        Test an authenticated endpoint.

        Ensure the request is forbidden without the correct permissions.
        """
        user = user or self.get_test_user()
        if isinstance(permission_codenames, str):
            permission_codenames = [permission_codenames]

        permissions = Permission.objects.filter(codename__in=permission_codenames)

        # Test anonymous request.
        response = self.assert_authenticated_anonymous_forbidden(
            method, *args, redirect_login=redirect_login, redirect_url=redirect_url, **kwargs
        )

        if user.is_anonymous:
            # Skip the permissions check.
            return response

        auth_client = self.get_authenticated_client(user)
        method_auth = getattr(auth_client, method)

        if permission_codenames:
            # Authenticated request without permission.
            user.user_permissions.remove(*permissions)
            response = method_auth(*args, **kwargs)
            self.assertIn(response.status_code, [400, 401, 403])

        # Authenticated request with permission.
        user.user_permissions.add(*permissions)
        response = method_auth(*args, **kwargs)
        self.assertEqual(response_code, response.status_code)
        user.user_permissions.remove(*permissions)

        return response

    def assertQuerySetEqual(self, first, second, description):
        """Compare two querysets."""
        self.assertEqual(set(first), set(second), description)


class KCIDBTestCase(TestCase):
    """Test helpers for KCIDB tests."""

    @staticmethod
    def _add_user_to_groups(user, groups):
        """Add user to the necessary groups."""
        for group_name in groups:
            user.groups.add(
                auth_models.Group.objects.get(name=group_name)
            )

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_revisions = models.KCIDBRevision.objects.filter(tree__id__in=self.trees_authorized[method])
        no_auth_revisions = models.KCIDBRevision.objects.exclude(tree__id__in=self.trees_authorized[method])

        auth_builds = models.KCIDBBuild.objects.filter(revision__in=auth_revisions)
        no_auth_builds = models.KCIDBBuild.objects.filter(revision__in=no_auth_revisions)

        auth_tests = models.KCIDBTest.objects.filter(build__in=auth_builds)
        no_auth_tests = models.KCIDBTest.objects.filter(build__in=no_auth_builds)

        checks = [
            (auth_revisions, 'No authorized revisions'),
            (no_auth_revisions, 'No unauthorized revisions'),
            (auth_builds, 'No authorized builds'),
            (no_auth_builds, 'No unauthorized builds'),
            (auth_tests, 'No authorized tests'),
            (no_auth_tests, 'No unauthorized tests'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def setUp(self):
        """Set up generic things. Query everything expecting the user authorization to change."""
        if self.anonymous:
            self.user = auth_models.AnonymousUser()
        else:
            self.user = auth_models.User.objects.get(username='test_user')
            self.client = self.get_authenticated_client(self.user)
            self._add_user_to_groups(self.user, self.groups)

        self.trees_authorized = self._get_authorized_gittree_ids(self.user)
        self.issues_authorized = self._get_authorized_issue_ids(self.user)
        self.policies_authorized = self._get_authorized_policy_ids(self.user)

    @staticmethod
    def _get_authorized_gittree_ids(user):
        """Get the gittree ids the user is authorized to read and write to."""
        user_authorized_gittree_ids = {}
        group_ids = [g.id for g in user.groups.all()]
        for method in ('read', 'write'):
            user_authorized_gittree_ids[method] = list(
                models.GitTree.objects
                .exclude(policy=None)
                .filter(
                    Q(**{f'policy__{method}_group': None}) |
                    Q(**{f'policy__{method}_group__id__in': group_ids})
                )
                .values_list('id', flat=True)
            )

        return user_authorized_gittree_ids

    @staticmethod
    def _get_authorized_issue_ids(user):
        """Get the issue ids the user is authorized to read and write to."""
        user_authorized_issue_ids = {}
        group_ids = [g.id for g in user.groups.all()]

        for method in ('read', 'write'):
            user_authorized_issue_ids[method] = list(
                models.Issue.objects
                .exclude(policy=None)
                .filter(
                    Q(**{f'policy__{method}_group': None}) |
                    Q(**{f'policy__{method}_group__id__in': group_ids})
                )
                .values_list('id', flat=True)
            )

        return user_authorized_issue_ids

    @staticmethod
    def _get_authorized_policy_ids(user):
        """Get the policy ids the user is authorized to read and write to."""
        user_authorized_policy_ids = {}
        group_ids = [g.id for g in user.groups.all()]

        for method in ('read', 'write'):
            user_authorized_policy_ids[method] = list(
                models.Policy.objects
                .filter(
                    Q(**{f'{method}_group': None}) |
                    Q(**{f'{method}_group__id__in': group_ids})
                )
                .values_list('id', flat=True)
            )

        return user_authorized_policy_ids
