"""Test the models module."""
from unittest import mock

from datawarehouse import models
from tests import utils


class ModelsTestCase(utils.TestCase):
    """Unit tests for the models module."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/fixtures/patches.yaml',
    ]

    def test_patch_gui_url(self):
        """Test that the GUI URLs are correctly provided by the model."""
        self.assertEqual(models.Patch.objects.first().gui_url, 'url 1')


class TestIssue(utils.TestCase):
    """Test issue model."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/kcidb/fixtures/issues.yaml',
    ]

    def setUp(self):
        """Set up data."""
        self.revision = models.KCIDBRevision.objects.get(iid=1)
        self.build_1 = models.KCIDBBuild.objects.get(iid=1)
        self.build_2 = models.KCIDBBuild.objects.get(iid=2)
        self.test_1 = models.KCIDBTest.objects.get(iid=1)
        self.test_2 = models.KCIDBTest.objects.get(iid=2)

        self.issue = models.Issue.objects.get(id=1)

    def test_revisions_empty(self):
        """Test revisions property."""
        self.assertEqual(0, self.issue.revisions.count())

    def test_revisions_direct(self):
        """Test revisions property."""
        self.revision.issues.set([self.issue])
        self.assertEqual(
            'public_revision',
            self.issue.revisions.get().id,
        )

    def test_revisions_indirect_build(self):
        """Test revisions property."""
        self.build_1.issues.set([self.issue])
        self.assertEqual(
            'public_revision',
            self.issue.revisions.get().id,
        )

        # Another build shouldn't change anything, it's already related.
        self.build_2.issues.set([self.issue])
        self.assertEqual(
            'public_revision',
            self.issue.revisions.get().id,
        )

    def test_revisions_indirect_test(self):
        """Test revisions property."""
        self.test_1.issues.set([self.issue])
        self.assertEqual(
            'public_revision',
            self.issue.revisions.get().id,
        )

        # Another test shouldn't change anything, it's already related.
        self.test_2.issues.set([self.issue])
        self.assertEqual(
            'public_revision',
            self.issue.revisions.get().id,
        )

    def test_revisions_mixed(self):
        """Test revisions property."""
        self.revision.issues.set([self.issue])
        self.build_1.issues.set([self.issue])
        self.test_1.issues.set([self.issue])
        self.assertEqual(
            'public_revision',
            self.issue.revisions.get().id,
        )


class TestTestMaintainer(utils.TestCase):
    """Test TestMaintainer model."""

    def test_create_from_address(self):
        """Test create_from_address method."""
        test_cases = [
            ('Cosme Fulanito <cosme@fulanito.com>,', 'Cosme Fulanito', 'cosme@fulanito.com'),
            ('Cosme Fulanito <cosme@fulanito.com>', 'Cosme Fulanito', 'cosme@fulanito.com'),
            ('Cosme <cosme@fulanito.com>', 'Cosme', 'cosme@fulanito.com'),
            ('<cosme@fulanito.com>', '', 'cosme@fulanito.com'),
            ('cosme@fulanito.com', '', 'cosme@fulanito.com'),
        ]

        for string, name, email in test_cases:
            maintainer = models.TestMaintainer.create_from_address(string)
            self.assertEqual(maintainer.name, name, string)
            self.assertEqual(maintainer.email, email, string)


class TestBeakerTask(utils.TestCase):
    """Test BeakerTask model."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def setUp(self):
        """Set up."""
        self.task = models.BeakerTask.objects.create(
            task_id=1,
            recipe_id=2
        )

    @mock.patch('datawarehouse.models.test_models.settings.BEAKER_URL', 'https://beaker')
    def test_recipe_url(self):
        """Test recipe_url property."""
        self.assertEqual('https://beaker/recipes/2', self.task.recipe_url)

    @mock.patch('datawarehouse.models.test_models.settings.BEAKER_URL', 'https://beaker')
    def test_task_url(self):
        """Test task_url property."""
        self.assertEqual('https://beaker/recipes/2#task1', self.task.task_url)

    def test_create_from_misc(self):
        """Test create_from_misc."""
        test = models.KCIDBTest.objects.first()
        task = models.BeakerTask.create_from_misc(
            test,
            {'beaker': {'task_id': 123, 'recipe_id': 321}}
        )

        self.assertEqual(123, task.task_id)
        self.assertEqual(321, task.recipe_id)
        self.assertEqual(test, task.kcidb_test)


class TestPipeline(utils.TestCase):
    """Test Pipeline model."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/pipeline_kcidb.yaml',
    ]

    def test_revision(self):
        """Test revision property."""
        pipeline = models.Pipeline.objects.first()
        revision = models.KCIDBRevision.objects.get(gitlabjob__pipeline=pipeline)

        self.assertEqual(pipeline.revision, revision)

    def test_web_url(self):
        """Test web_url property."""
        pipeline = models.Pipeline.objects.get(pipeline_id=1)
        self.assertEqual(
            'https://gitlab.com/cki-project/brew-pipeline/-/pipelines/1',
            pipeline.web_url
        )


class TestProject(utils.TestCase):
    """Test Project model."""

    fixtures = [
        'tests/fixtures/basic.yaml',
    ]

    def test_web_url(self):
        """Test web_url property."""
        project = models.Project.objects.get(id=1)
        self.assertEqual(
            'https://gitlab.com/cki-project/brew-pipeline',
            project.web_url
        )


class TestGitlabJob(utils.TestCase):
    """Test GitlabJob model."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/pipeline_kcidb.yaml',
    ]

    def test_web_url(self):
        """Test web_url property."""
        gitlab_job = models.GitlabJob.objects.get(job_id=1)
        self.assertEqual(
            'https://gitlab.com/cki-project/brew-pipeline/-/jobs/1',
            gitlab_job.web_url
        )
