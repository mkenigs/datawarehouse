"""Test templatags."""
from unittest import mock

from django.http.request import QueryDict

from datawarehouse import models
from datawarehouse.templatetags import trim_queryset
from datawarehouse.templatetags import url_replace
from tests import utils


class TestTrimQueryset(utils.TestCase):
    """Test trim_queryset templatetags."""

    def test_trim_queryset(self):
        """Test trim_queryset templatetag."""
        models.GitTree.objects.bulk_create([
            models.GitTree(name=name)
            for name in range(15)
        ])

        queryset = models.GitTree.objects.all().order_by('id')

        # Test default length: 10
        trimmed = trim_queryset.trim_queryset(queryset)
        self.assertEqual(10, len(trimmed))
        self.assertEqual(5, trimmed.trimmed_elements)
        self.assertListEqual(
            [item.name for item in trimmed],
            [str(item) for item in range(10)]
        )

        # Test length as parameter
        trimmed = trim_queryset.trim_queryset(queryset, trim_at=5)
        self.assertEqual(5, len(trimmed))
        self.assertEqual(10, trimmed.trimmed_elements)
        self.assertListEqual(
            [item.name for item in trimmed],
            [str(item) for item in range(5)]
        )


class TestUrlReplace(utils.TestCase):
    """Test url_replace templatetags."""

    def test_url_replace_none(self):
        """Test url_replace without kwargs."""
        query = QueryDict('foo=foo&bar=bar')

        context = {'request': mock.Mock(GET=query)}

        new_url = url_replace.url_replace(context)
        self.assertEqual('foo=foo&bar=bar', new_url)

    def test_url_replace(self):
        """Test url_replace with kwargs. If the value exists it should replace it."""
        query = QueryDict('foo=foo&bar=bar')

        context = {'request': mock.Mock(GET=query)}

        new_url = url_replace.url_replace(context, bar='barbar')
        self.assertEqual('foo=foo&bar=barbar', new_url)

    def test_url_add(self):
        """Test url_replace with kwargs. If the value doesn't exit it should add it."""
        query = QueryDict('foo=foo')

        context = {'request': mock.Mock(GET=query)}

        new_url = url_replace.url_replace(context, bar='bar')
        self.assertEqual('foo=foo&bar=bar', new_url)
