# pylint: disable=too-many-lines
"""Test model creation from KCIDB data."""
import json
import os
import pathlib
from unittest import mock

from django.db.models.query import QuerySet
import responses

from datawarehouse import models
from tests import utils


def load_json(name):
    """Load json file from assets."""
    file_name = os.path.join(utils.ASSETS_DIR, name)
    file_content = pathlib.Path(file_name).read_text()

    return json.loads(file_content)


def mock_patch():
    """Mock patch requests."""
    patch_body = """MIME-Version: 1.0
Subject: [RHEL PATCH 206/206] fix some stuff, and break some other
commit 56887cffe946bb0a90c74429fa94d6110a73119d
Author: Patch Author <patch@author.com>
Date:   Mon Feb 22 10:48:09 2021 +0100

    fix some stuff, and break some other
    """
    responses.add(responses.GET, 'http://patchwork.server/patch/2322797/mbox/', body=patch_body)


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestPipeline(utils.TestCase):
    """Test creation of Pipeline model."""

    def test_create_pipeline_models(self):
        """Check pipeline model is created correctly."""
        misc = {
            'job': {
                'id': 887316,
                'name': 'merge',
                'stage': 'merge',
                'started_at': '2020-06-03T15:11:19.327Z',
                'created_at': '2020-06-03T15:08:05.512Z',
                'finished_at': '2020-06-03T15:14:55.148Z',
                'duration': 215.820987,
                'test_hash': 'a8bf807e5ba0e6ff2613ae0cf14ac439480b9073',
                'tag': '-209.el8',
                'commit_message_title': '[redhat] kernel',
                'kernel_version': None
            },
            'pipeline': {
                'id': 592705,
                'variables': {
                    'cki_pipeline_type': 'patchwork',
                    'kernel_type': 'internal'
                },
                'started_at': '2020-06-03T15:08:09.957Z',
                'created_at': '2020-06-03T15:08:05.288Z',
                'finished_at': None,
                'duration': None,
                'ref': 'rhel8',
                'sha': 'c7ff7a4def290d6f7d33b3d15782b4e325bf2aa5',
                'project': {
                    'id': 2,
                    'path_with_namespace': 'cki-project/cki-pipeline',
                    'instance_url': 'https://gitlab.com'
                }
            }
        }

        pipeline = models.Pipeline.create_from_misc(misc)
        self.assertEqual(2, pipeline.project.project_id)
        self.assertEqual('cki-project/cki-pipeline', pipeline.project.path)
        self.assertEqual('https://gitlab.com', pipeline.project.instance_url)
        self.assertEqual('rhel8', pipeline.gittree.name)
        self.assertEqual('c7ff7a4def290d6f7d33b3d15782b4e325bf2aa5', pipeline.commit_id)
        self.assertEqual('2020-06-03T15:08:09.957Z', pipeline.started_at)
        self.assertEqual('2020-06-03T15:08:05.288Z', pipeline.created_at)
        self.assertEqual(None, pipeline.finished_at)
        self.assertEqual(None, pipeline.duration)
        self.assertEqual('a8bf807e5ba0e6ff2613ae0cf14ac439480b9073', pipeline.test_hash)
        self.assertEqual('-209.el8', pipeline.tag)
        self.assertEqual('[redhat] kernel', pipeline.commit_message_title)
        self.assertEqual(None, pipeline.kernel_version)

        self.assertDictEqual(
            {'cki_pipeline_type': 'patchwork', 'kernel_type': 'internal'},
            pipeline.trigger_variables
        )

        # New values, try updating it.
        misc['pipeline']['finished_at'] = '2020-06-03T15:08:20.000Z'
        misc['pipeline']['duration'] = 123.45
        pipeline = models.Pipeline.create_from_misc(misc)
        self.assertEqual('2020-06-03T15:08:20.000Z', pipeline.finished_at)
        self.assertEqual(123.45, pipeline.duration)


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestKCIDBRevisionFromJson(utils.TestCase):
    """Test creation of KCIDBRevision model."""

    def test_basic(self):
        """Submit only id."""
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3+'
                   '14f1076637e351743158c458afa5ee5032dc26844a4c923dabb4846c3d0fa197'),
            'origin': 'redhat',
        }
        revision = models.KCIDBRevision.create_from_json(data)
        self.assertIsInstance(revision, models.KCIDBRevision)

    def test_re_submit(self):
        """Submit twice. Updates current data."""
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
        }
        revision = models.KCIDBRevision.create_from_json(data)
        self.assertIsNone(revision.message_id)

        # Re submit with new data.
        data['message_id'] = '<foo@bar.com>'
        revision = models.KCIDBRevision.create_from_json(data)
        self.assertEqual(revision.message_id, data['message_id'])

        # Re submit with new data, without the previous value.
        del data['message_id']
        data['valid'] = True
        revision = models.KCIDBRevision.create_from_json(data)
        self.assertIsNotNone(revision.message_id)
        self.assertEqual(revision.valid, data['valid'])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBRevision.objects.filter(id=data['id']).count()
        )

    @responses.activate
    def test_patch(self):
        """Submit email patches."""
        body = (
            b'From foo@baz Mon 25 Nov 2019 02:27:19 PM CET\n'
            b'From: Some One <some-one@redhat.com>\n'
            b'Date: Tue, 19 Nov 2019 23:47:33 +0100\n'
            b'Subject: fix something somewhere\n'
            b'..')
        responses.add(responses.GET, 'http://some-patch.server/patch/1234', body=body)
        responses.add(responses.GET, 'http://some-patch.server/patch/1235', body=body)
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'patch_mboxes': [
                {'url': 'http://some-patch.server/patch/1234', 'name': '1234'},
                {'url': 'http://some-patch.server/patch/1235', 'name': '1235'}
            ],
        }

        revision = models.KCIDBRevision.create_from_json(data)
        self.assertEqual(2, revision.patches.count())
        self.assertFalse(hasattr(revision.patches.first(), 'patchworkpatch'))

    def test_log_url(self):
        """Submit log_url."""
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'log_url': 'http://log.server/log.name',
        }

        revision = models.KCIDBRevision.create_from_json(data)

        self.assertIsInstance(revision.log, models.Artifact)
        self.assertEqual('http://log.server/log.name', revision.log.url)
        self.assertEqual('log.name', revision.log.name)

    def test_contacts(self):
        """Test contact submission."""
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'contacts': ['someone@email.com', 'Some Other <some-other@mail.com>']
        }

        revision = models.KCIDBRevision.create_from_json(data)

        self.assertEqual(2, revision.contacts.count())
        contact_1 = revision.contacts.get(email='someone@email.com')
        self.assertEqual('', contact_1.name)
        contact_2 = revision.contacts.get(email='some-other@mail.com')
        self.assertEqual('Some Other', contact_2.name)

    @responses.activate
    def test_all_data(self):
        """Check it creates all the pipeline models."""
        mock_patch()
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'tree_name': 'arm',
            'git_repository_url': 'https://repository.com/repo/kernel-rhel',
            'git_repository_branch': 'rhel-1.2.3',
            'git_commit_hash': '403cbf29a4e277ad4872515ec3854b175960bbdf',
            'git_commit_name': 'commit name',
            'patch_mboxes': [{'url': 'http://patchwork.server/patch/2322797/mbox/',
                              'name': 'mbox'}],
            'message_id': '<e41888bcd8ecf2e9bc8cc37c56386e01a5b43c56.some-one@redhat.com>',
            'description': 'this is the description',
            'publishing_time': '2020-06-01T06:47:41.108Z',
            'discovery_time': '2020-06-01T06:47:41.108Z',
            'valid': True,
            'contacts': ['someone@email.com', 'Some Other <some-other@mail.com>'],
            'log_url': 'http://log.server/log.name',
            'misc': {'pipeline': {'variables': {'cki_pipeline_type': 'patchwork'}}},
        }

        revision = models.KCIDBRevision.create_from_json(data)

        self.assertEqual(
            ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
             'decd6167bf4f6bec1284006d0522381b44660df3'),
            revision.id
        )
        self.assertIsInstance(revision.origin, models.KCIDBOrigin)
        self.assertEqual('redhat', revision.origin.name)

        self.assertIsInstance(revision.tree, models.GitTree)
        self.assertEqual('arm', revision.tree.name)

        self.assertEqual('https://repository.com/repo/kernel-rhel', revision.git_repository_url)
        self.assertEqual('rhel-1.2.3', revision.git_repository_branch)
        self.assertEqual('403cbf29a4e277ad4872515ec3854b175960bbdf', revision.git_commit_hash)
        self.assertEqual('commit name', revision.git_commit_name)
        self.assertEqual('<e41888bcd8ecf2e9bc8cc37c56386e01a5b43c56.some-one@redhat.com>', revision.message_id)
        self.assertEqual('this is the description', revision.description)
        self.assertEqual(True, revision.valid)

        # These two are strings because it's comparing with the created object, not from a query.
        self.assertEqual('2020-06-01T06:47:41.108Z', revision.publishing_time)
        self.assertEqual('2020-06-01T06:47:41.108Z', revision.discovery_time)

    @responses.activate
    def test_create_pipeline_models(self):
        """Check it creates all the pipeline models."""
        mock_patch()
        data = {
            'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                   'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'patch_mboxes': [{'url': 'http://patchwork.server/patch/2322797/mbox/',
                              'name': 'mbox'}],
            'log_url': 'http://log.server/log.name',
            'valid': True,
            'misc': {
                'job': {
                    'id': 887316,
                    'name': 'merge',
                    'stage': 'merge',
                    'started_at': '2020-06-03T15:11:19.327Z',
                    'created_at': '2020-06-03T15:08:05.512Z',
                    'finished_at': '2020-06-03T15:14:55.148Z',
                    'duration': 215.820987,
                    'test_hash': 'a8bf807e5ba0e6ff2613ae0cf14ac439480b9073',
                    'tag': '-209.el8',
                    'commit_message_title': '[redhat] kernel',
                    'kernel_version': '1.2.3'
                },
                'pipeline': {
                    'id': 592705,
                    'variables': {
                        'cki_pipeline_type': 'patchwork',
                    },
                    'started_at': '2020-06-03T15:08:09.957Z',
                    'created_at': '2020-06-03T15:08:05.288Z',
                    'finished_at': None,
                    'duration': None,
                    'ref': 'rhel8',
                    'sha': 'c7ff7a4def290d6f7d33b3d15782b4e325bf2aa5',
                    'project': {
                        'id': 2,
                        'path_with_namespace': 'cki-project/cki-pipeline',
                        'instance_url': 'https://gitlab.com'
                    }
                }
            }
        }

        revision = models.KCIDBRevision.create_from_json(data)
        gitlab_job = revision.gitlabjob_set.first()

        self.assertIsNotNone(gitlab_job)
        self.assertIsNotNone(gitlab_job.pipeline)
        self.assertEqual(887316, gitlab_job.job_id)
        self.assertEqual(592705, gitlab_job.pipeline.pipeline_id)

        self.assertTrue(gitlab_job.pipeline.patches.count())
        self.assertEqual(
            sorted([patch.url for patch in revision.patches.all()]),
            sorted([patch.url for patch in gitlab_job.pipeline.patches.all()])
        )

        self.assertEqual('1.2.3', revision.kernel_version)


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestKCIDBBuildFromJson(utils.TestCase):
    """Test creation of KCIDBBuild model."""

    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set Up."""
        models.KCIDBRevision.create_from_json(
            {
                'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git'
                       '@decd6167bf4f6bec1284006d0522381b44660df3'),
                'origin': 'redhat',
            }
        )

    def test_basic(self):
        """Submit only id."""
        data = {
            'revision_id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                            'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'id': 'redhat:887318',
        }
        build = models.KCIDBBuild.create_from_json(data)

        self.assertIsInstance(build, models.KCIDBBuild)
        self.assertIsInstance(build.revision, models.KCIDBRevision)
        self.assertEqual(
            ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
             'decd6167bf4f6bec1284006d0522381b44660df3'),
            build.revision.id
        )
        self.assertEqual('redhat:887318', build.id)
        self.assertIsInstance(build.origin, models.KCIDBOrigin)
        self.assertEqual('redhat', build.origin.name)

    def test_re_submit(self):
        """Submit submit multiple times. Updates current data."""
        data = {
            'revision_id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                            'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'id': 'redhat:887318',
        }
        build = models.KCIDBBuild.create_from_json(data)
        self.assertIsNone(build.valid)

        # Re submit with new data.
        data['valid'] = False
        build = models.KCIDBBuild.create_from_json(data)
        self.assertEqual(build.valid, data['valid'])

        # Re submit with new data, without the previous value.
        del data['valid']
        data['description'] = 'foobar'
        build = models.KCIDBBuild.create_from_json(data)
        self.assertIsNotNone(build.valid)
        self.assertEqual(build.description, data['description'])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBBuild.objects.filter(id=data['id']).count()
        )

    def test_submit_files(self):
        """Test files submission. log_url, input_files and output_files."""
        data = {
            'revision_id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                            'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': 'http://log.server/log.name',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        build = models.KCIDBBuild.create_from_json(data)

        # log_url
        self.assertIsInstance(build.log, models.Artifact)
        self.assertEqual('http://log.server/log.name', build.log.url)
        self.assertEqual('log.name', build.log.name)

        # input_files
        self.assertListEqual(
            [{'url': 'http://log.server/input.file', 'name': 'input.file'},
             {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'}],
            [{'url': file.url, 'name': file.name} for file in build.input_files.all()]
        )

        # output_files
        self.assertListEqual(
            [{'url': 'http://log.server/output.file', 'name': 'output.file'},
             {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'}],
            [{'url': file.url, 'name': file.name} for file in build.output_files.all()]
        )

    def test_all_data(self):
        """Test complete object submission."""
        data = {
            'revision_id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                            'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': 'http://log.server/log.name',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
            'start_time': '2020-06-03T15:14:57.215Z',
            'duration': 632,
            'architecture': 'aarch64',
            'command': 'make rpmbuild ...',
            'compiler': 'aarch64-linux-gnu-gcc (GCC) 8.2.1 20181105 (Red Hat Cross 8.2.1-1)',
            'config_name': 'fedora',
            'valid': True,
        }
        build = models.KCIDBBuild.create_from_json(data)

        self.assertEqual('2020-06-03T15:14:57.215Z', build.start_time)
        self.assertEqual(632, build.duration)
        self.assertEqual('make rpmbuild ...', build.command)
        self.assertEqual('fedora', build.config_name)
        self.assertEqual(True, build.valid)

        self.assertIsInstance(build.architecture, models.ArchitectureEnum)
        self.assertEqual('aarch64', build.architecture.name)
        self.assertIsInstance(build.compiler, models.Compiler)
        self.assertEqual('aarch64-linux-gnu-gcc (GCC) 8.2.1 20181105 (Red Hat Cross 8.2.1-1)', build.compiler.name)

    def test_create_pipeline_models(self):
        """Check it creates all the pipeline models."""
        data = {
            'revision_id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git@'
                            'decd6167bf4f6bec1284006d0522381b44660df3'),
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': 'http://log.server/log.name',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
            'start_time': '2020-06-03T15:14:57.215Z',
            'duration': 632,
            'architecture': 'aarch64',
            'command': 'make rpmbuild ..',
            'compiler': 'aarch64-linux-gnu-gcc (GCC) 8.2.1 20181105 (Red Hat Cross 8.2.1-1)',
            'config_name': 'fedora',
            'valid': True,
            'misc': {
                'job': {
                    'id': 887318,
                    'name': 'build aarch64',
                    'stage': 'build',
                    'started_at': '2020-06-03T15:14:57.215Z',
                    'created_at': '2020-06-03T15:08:05.615Z',
                    'finished_at': '2020-06-03T15:27:00.666Z',
                    'duration': 723.450641,
                    'test_hash': 'a8bf807e5ba0e6ff2613ae0cf14ac439480b9073',
                    'tag': '-209.el8',
                    'commit_message_title': '[redhat] kernel-4.18.0-209.el8',
                    'kernel_version': '4.18.0-209.el8.cki'
                },
                'pipeline': {
                    'id': 592705,
                    'variables': {
                        'cki_pipeline_type': 'patchwork',
                    },
                    'started_at': '2020-06-03T15:08:09.957Z',
                    'created_at': '2020-06-03T15:08:05.288Z',
                    'finished_at': '2020-06-04T00:50:23.262Z',
                    'duration': 34916,
                    'ref': 'rhel8',
                    'sha': 'c7ff7a4def290d6f7d33b3d15782b4e325bf2aa5',
                    'project': {
                        'id': 2,
                        'path_with_namespace': 'cki-project/cki-pipeline',
                        'instance_url': 'https://gitlab.com'
                    }
                }
            }
        }

        build = models.KCIDBBuild.create_from_json(data)
        gitlab_job = build.gitlabjob_set.first()

        self.assertIsNotNone(gitlab_job)
        self.assertIsNotNone(gitlab_job.pipeline)
        self.assertEqual(887318, gitlab_job.job_id)
        self.assertEqual(592705, gitlab_job.pipeline.pipeline_id)


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestKCIDBTestFromJson(utils.TestCase):
    """Test creation of KCIDBTest model."""

    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set Up."""
        models.KCIDBRevision.create_from_json(
            {
                'id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git'
                       '@decd6167bf4f6bec1284006d0522381b44660df3'),
                'origin': 'redhat',
            }
        )
        models.KCIDBBuild.create_from_json(
            {
                'revision_id': ('https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git'
                                '@decd6167bf4f6bec1284006d0522381b44660df3'),
                'origin': 'redhat',
                'id': 'redhat:887318',
                'architecture': 'aarch64',
            }
        )

    def test_basic(self):
        """Submit only id."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        test = models.KCIDBTest.create_from_json(data)

        self.assertIsInstance(test, models.KCIDBTest)
        self.assertIsInstance(test.build, models.KCIDBBuild)
        self.assertEqual('redhat:887318', test.build.id)

        # These are False by default
        self.assertFalse(test.targeted)
        self.assertFalse(test.kernel_debug)

    def test_re_submit(self):
        """Submit multiple times. Updates current data."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        test = models.KCIDBTest.create_from_json(data)
        self.assertIsNone(test.waived)

        # Re submit with new data.
        data['waived'] = True
        test = models.KCIDBTest.create_from_json(data)
        self.assertEqual(test.waived, data['waived'])

        # Re submit with new data, without the previous value.
        del data['waived']
        data['duration'] = 123
        test = models.KCIDBTest.create_from_json(data)
        self.assertIsNotNone(test.waived)
        self.assertEqual(test.duration, data['duration'])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBTest.objects.filter(id=data['id']).count()
        )

    def test_output_files(self):
        """Check created artifacts for output_files."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        test = models.KCIDBTest.create_from_json(data)

        self.assertListEqual(
            [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
            [{'url': file.url, 'name': file.name} for file in test.output_files.all()]
        )

    def test_maintainers_empty(self):
        """Test maintainers field as comma separated string."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'description': 'Boot test',
            'misc': {'maintainers': None}
        }
        models.KCIDBTest.create_from_json(data)

    def test_maintainers_string(self):
        """Test maintainers field as comma separated string."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'description': 'Boot test',
            'misc': {
                'maintainers': (
                    'Maintainer One <one@maintainer.com>,'
                    '<two@maintainer.com>,'
                    'three@maintainer.com'
                )
            }
        }
        test = models.KCIDBTest.create_from_json(data)

        cases = [
            ('Maintainer One', 'one@maintainer.com'),
            ('', 'two@maintainer.com'),
            ('', 'three@maintainer.com'),
        ]

        for name, email in cases:
            self.assertTrue(
                test.test.maintainers.filter(name=name, email=email).exists(),
                (name, email)
            )

    def test_maintainers_list(self):
        """Test maintainers field as list."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'description': 'Boot test',
            'misc': {
                'maintainers': [
                    'Maintainer One <one@maintainer.com>',
                    '<two@maintainer.com>',
                    'three@maintainer.com',
                ]
            }
        }
        test = models.KCIDBTest.create_from_json(data)

        cases = [
            ('Maintainer One', 'one@maintainer.com'),
            ('', 'two@maintainer.com'),
            ('', 'three@maintainer.com'),
        ]

        for name, email in cases:
            self.assertTrue(
                test.test.maintainers.filter(name=name, email=email).exists(),
                (name, email)
            )

    def test_all_data(self):
        """Test complete object submission."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'environment': {
                'description': 'hostname.redhat.com'
            },
            'path': 'boot',
            'description': 'Boot test',
            'waived': False,
            'start_time': '2020-06-03T15:52:25Z',
            'duration': 158,
            'status': 'PASS',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
            ],
            'misc': {
                'maintainers': 'Cosme Fulanito <cosme@fulanito.com>, Pepe <pe@pe.com>',
                'debug': True,
                'targeted': True,
            }
        }
        test = models.KCIDBTest.create_from_json(data)

        self.assertIsInstance(test.environment, models.BeakerResource)
        self.assertEqual('hostname.redhat.com', test.environment.fqdn)

        self.assertIsInstance(test.status, models.ResultEnum)
        self.assertEqual('PASS', test.status.name)

        self.assertIsInstance(test.test, models.Test)
        self.assertEqual('boot', test.test.universal_id)
        self.assertEqual('Boot test', test.test.name)
        self.assertEqual(2, test.test.maintainers.count())
        self.assertTrue(
            test.test.maintainers.filter(name='Cosme Fulanito', email='cosme@fulanito.com').exists()
        )
        self.assertTrue(
            test.test.maintainers.filter(name='Pepe', email='pe@pe.com').exists()
        )

        self.assertEqual(False, test.waived)
        self.assertEqual('2020-06-03T15:52:25Z', test.start_time)
        self.assertEqual(158, test.duration)

        self.assertEqual(1, test.output_files.count())
        self.assertTrue(test.kernel_debug)
        self.assertTrue(test.targeted)

    def test_create_pipeline_models(self):
        """Check it creates all the pipeline models."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'environment': {
                'description': 'hostname.redhat.com'
            },
            'path': 'boot',
            'description': 'Boot test',
            'waived': False,
            'start_time': '2020-06-03T15:52:25Z',
            'duration': 158,
            'status': 'PASS',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
            ],
            'misc': {
                'debug': False,
                'targeted': True,
                'job': {
                    'id': 887330,
                    'name': 'test aarch64',
                    'stage': 'test',
                    'started_at': '2020-06-03T15:41:43.940Z',
                    'created_at': '2020-06-03T15:08:06.237Z',
                    'finished_at': '2020-06-03T18:02:53.430Z',
                    'duration': 8469.490879,
                    'test_hash': 'a8bf807e5ba0e6ff2613ae0cf14ac439480b9073',
                    'tag': '-209.el8',
                    'commit_message_title': '[redhat] kernel-4.18.0-209.el8',
                    'kernel_version': '4.18.0-209.el8.cki'},
                'pipeline': {
                    'id': 592705,
                    'variables': {
                        'cki_pipeline_type': 'patchwork',
                    },
                    'started_at': '2020-06-03T15:08:09.957Z',
                    'created_at': '2020-06-03T15:08:05.288Z',
                    'finished_at': '2020-06-04T00:50:23.262Z',
                    'duration': 34916,
                    'ref': 'rhel8',
                    'sha': 'c7ff7a4def290d6f7d33b3d15782b4e325bf2aa5',
                    'project': {
                        'id': 2,
                        'path_with_namespace': 'cki-project/cki-pipeline',
                        'instance_url': 'https://gitlab.com'
                    }
                }
            }
        }
        test = models.KCIDBTest.create_from_json(data)
        gitlab_job = test.gitlabjob_set.first()

        self.assertIsNotNone(gitlab_job)
        self.assertIsNotNone(gitlab_job.pipeline)
        self.assertEqual(887330, gitlab_job.job_id)
        self.assertEqual(592705, gitlab_job.pipeline.pipeline_id)

    def test_create_pipeline_models_beaker(self):
        """Check it creates all the pipeline models."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'environment': {
                'description': 'hostname.redhat.com'
            },
            'path': 'boot',
            'description': 'Boot test',
            'waived': False,
            'start_time': '2020-06-03T15:52:25Z',
            'duration': 158,
            'status': 'PASS',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
            ],
            'misc': {
                'debug': False,
                'targeted': True,
                'beaker': {
                    'task_id': 111218982,
                    'recipe_id': 8378739,
                    'finish_time': '2020-06-03T16:46:05+00:00',
                    'retcode': 0,
                },
                'job': {
                    'id': 887330,
                    'name': 'test aarch64',
                    'stage': 'test',
                    'started_at': '2020-06-03T15:41:43.940Z',
                    'created_at': '2020-06-03T15:08:06.237Z',
                    'finished_at': '2020-06-03T18:02:53.430Z',
                    'duration': 8469.490879,
                    'test_hash': 'a8bf807e5ba0e6ff2613ae0cf14ac439480b9073',
                    'tag': '-209.el8',
                    'commit_message_title': '[redhat] kernel-4.18.0-209.el8',
                    'kernel_version': '4.18.0-209.el8.cki'},
                'pipeline': {
                    'id': 592705,
                    'variables': {
                        'cki_pipeline_type': 'patchwork',
                    },
                    'started_at': '2020-06-03T15:08:09.957Z',
                    'created_at': '2020-06-03T15:08:05.288Z',
                    'finished_at': '2020-06-04T00:50:23.262Z',
                    'duration': 34916,
                    'ref': 'rhel8',
                    'sha': 'c7ff7a4def290d6f7d33b3d15782b4e325bf2aa5',
                    'project': {
                        'id': 2,
                        'path_with_namespace': 'cki-project/cki-pipeline',
                        'instance_url': 'https://gitlab.com'
                    }
                }
            }
        }
        test = models.KCIDBTest.create_from_json(data)
        gitlab_job = test.gitlabjob_set.first()

        self.assertIsNotNone(gitlab_job)
        self.assertIsNotNone(gitlab_job.pipeline)
        self.assertEqual(887330, gitlab_job.job_id)
        self.assertEqual(592705, gitlab_job.pipeline.pipeline_id)

        beaker_task = test.beakertask_set.first()
        self.assertIsNotNone(beaker_task)

        self.assertEqual(111218982, beaker_task.task_id)
        self.assertEqual(8378739, beaker_task.recipe_id)


class TestKCIDBNotifications(utils.TestCase):
    """Test kcidb notifications are sent."""

    @staticmethod
    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_revision(signal):
        """Test send_kcidb_notification."""
        data = {
            'id': 'https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            'origin': 'redhat',
        }

        # Create call
        models.KCIDBRevision.create_from_json(data)
        # Update call
        rev = models.KCIDBRevision.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.revision.create_from_json',
                status='new',
                object_type='revision',
                objects=[rev]
            ),
            mock.call(
                sender='kcidb.revision.create_from_json',
                status='updated',
                object_type='revision',
                objects=[rev]
            ),
        ])

    @staticmethod
    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_build(signal):
        """Test send_kcidb_notification."""
        rev = models.KCIDBRevision.objects.create(
            id='https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'revision_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
        }

        # Create call
        models.KCIDBBuild.create_from_json(data)
        # Update call
        build = models.KCIDBBuild.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.create_from_json',
                status='new',
                object_type='build',
                objects=[build]
            ),
            mock.call(
                sender='kcidb.build.create_from_json',
                status='updated',
                object_type='build',
                objects=[build]
            )
        ])

    @staticmethod
    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_test(signal):
        """Test send_kcidb_notification."""
        rev = models.KCIDBRevision.objects.create(
            id='https://git.kernel.org/linux.git@decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            revision=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }

        # Create call
        models.KCIDBTest.create_from_json(data)
        # Update call
        test = models.KCIDBTest.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.create_from_json',
                status='new',
                object_type='test',
                objects=[test]
            ),
            mock.call(
                sender='kcidb.test.create_from_json',
                status='updated',
                object_type='test',
                objects=[test]
            )
        ])


class TestVisibility(utils.TestCase):
    """Test objects visibility."""

    @staticmethod
    def _create_pipeline():
        """Create Pipeline."""
        next_id = models.Pipeline.objects.count()
        return models.Pipeline.objects.create(
            pipeline_id=next_id,
            project=models.Project.objects.create(project_id=next_id, path='test-proj'),
            gittree=models.GitTree.objects.create(name=f'test-tree-{next_id}'),
        )

    @staticmethod
    def _create_gitlabjob(pipeline, revision=None, build=None, test=None):
        """Create MergeRun."""
        next_id = models.GitlabJob.objects.count()
        gitlab_job = models.GitlabJob.objects.create(
            job_id=next_id,
            pipeline=pipeline,
            kcidb_revision=revision,
        )
        if build:
            gitlab_job.kcidb_build.add(build)
        if test:
            gitlab_job.kcidb_test.add(test)

        return gitlab_job

    def test_revision(self):
        """Test revision visibility."""
        revision = models.KCIDBRevision.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        # By default is private.
        self.assertFalse(revision.is_public)

        # Add a pipeline without variables, still private.
        pipeline = self._create_pipeline()
        self._create_gitlabjob(pipeline, revision=revision)
        self.assertFalse(revision.is_public)

        # Adding the kernel_type=upstream variable makes it public.
        models.TriggerVariable.objects.create(
            pipeline=pipeline, key='kernel_type', value='upstream'
        )
        self.assertTrue(revision.is_public)

        # Add a new pipeline without kernel_type goes back to private.
        pipeline = self._create_pipeline()
        self._create_gitlabjob(pipeline, revision=revision)
        self.assertFalse(revision.is_public)

        # Adding the kernel_type with any other value keeps it private.
        models.TriggerVariable.objects.create(
            pipeline=pipeline, key='kernel_type', value='fernet'
        )
        self.assertFalse(revision.is_public)

    def test_build(self):
        """Test build visibility."""
        rev = models.KCIDBRevision.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        build = models.KCIDBBuild.create_from_json({
            'revision_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
        })

        # By default is private.
        self.assertFalse(build.is_public)

        # Add a pipeline without variables, still private.
        pipeline = self._create_pipeline()
        self._create_gitlabjob(pipeline, build=build)
        self.assertFalse(build.is_public)

        # Adding the kernel_type=upstream variable makes it public.
        models.TriggerVariable.objects.create(
            pipeline=pipeline, key='kernel_type', value='upstream'
        )
        self.assertTrue(build.is_public)

        # Add a new pipeline without kernel_type goes back to private.
        pipeline = self._create_pipeline()
        self._create_gitlabjob(pipeline, build=build)
        self.assertFalse(build.is_public)

        # Adding the kernel_type with any other value keeps it private.
        models.TriggerVariable.objects.create(
            pipeline=pipeline, key='kernel_type', value='fernet'
        )
        self.assertFalse(build.is_public)

    def test_test(self):
        """Test test visibility."""
        rev = models.KCIDBRevision.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            revision=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        test = models.KCIDBTest.create_from_json({
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        })

        # By default is private.
        self.assertFalse(test.is_public)

        # Add a pipeline without variables, still private.
        pipeline = self._create_pipeline()
        self._create_gitlabjob(pipeline, test=test)
        self.assertFalse(test.is_public)

        # Adding the kernel_type=upstream variable makes it public.
        models.TriggerVariable.objects.create(
            pipeline=pipeline, key='kernel_type', value='upstream'
        )
        self.assertTrue(test.is_public)

        # Add a new pipeline without kernel_type goes back to private.
        pipeline = self._create_pipeline()
        self._create_gitlabjob(pipeline, test=test)
        self.assertFalse(test.is_public)

        # Adding the kernel_type with any other value keeps it private.
        models.TriggerVariable.objects.create(
            pipeline=pipeline, key='kernel_type', value='fernet'
        )
        self.assertFalse(test.is_public)


class TestRevisionAggregated(utils.TestCase):
    """Test aggregated data on a revision."""

    def setUp(self):
        data = {
            'version': {'major': 3, 'minor': 0},
            'revisions': [
                {'origin': 'redhat', 'id': 'decd6167bf4f6bec1284006d0522381b44660df3', 'valid': False},
            ],
            'builds': [
                {'origin': 'redhat', 'revision_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-1', 'valid': False},
                {'origin': 'redhat', 'revision_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-2', 'valid': False},
                {'origin': 'redhat', 'revision_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-3', 'valid': True},
            ],
            'tests': [
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-1', 'status': 'FAIL'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-2', 'status': 'ERROR'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-3', 'status': 'PASS'},
            ]
        }
        self.assert_authenticated_post(
            201, 'add_kcidbrevision', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

    def _test_rev(self, cases, only_aggregated=False):
        """Test attrs on both rev and rev_aggregated."""
        rev_id = 'decd6167bf4f6bec1284006d0522381b44660df3'
        rev = models.KCIDBRevision.objects.get(id=rev_id)
        rev_aggregated = models.KCIDBRevision.objects.aggregated().get(id=rev_id)

        for attr, value in cases:
            if isinstance(value, QuerySet):
                check = self.assertQuerySetEqual
            else:
                check = self.assertEqual

            check(getattr(rev_aggregated, attr), value, attr)
            if not only_aggregated:
                check(getattr(rev, attr), value, attr)

    def test_nothing_triaged(self):
        """Test none objects were triaged."""
        rev = models.KCIDBRevision.objects.get(id='decd6167bf4f6bec1284006d0522381b44660df3')

        # Without aggregated call these methods are not available.
        self.assertFalse(hasattr(rev, 'stats_revision_triaged'))
        self.assertFalse(hasattr(rev, 'stats_revision_untriaged'))
        self.assertFalse(hasattr(rev, 'stats_tests_triaged'))
        self.assertFalse(hasattr(rev, 'stats_tests_untriaged'))
        self.assertFalse(hasattr(rev, 'stats_builds_triaged'))
        self.assertFalse(hasattr(rev, 'stats_builds_untriaged'))
        self.assertIsNone(rev.has_objects_missing_triage)
        self.assertIsNone(rev.has_objects_with_issues)

        # No issues but failed jobs. All untriaged, no triaged.
        cases = [
            ('stats_revision_untriaged', True),
            ('stats_builds_untriaged', True),
            ('stats_tests_untriaged', True),
            ('stats_revision_triaged', False),
            ('stats_builds_triaged', False),
            ('stats_tests_triaged', False),
            # No issues, missing triage.
            ('has_objects_missing_triage', True),
            ('has_objects_with_issues', False),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_triaged', models.KCIDBBuild.objects.none()),
            ('builds_untriaged', models.KCIDBBuild.objects.filter(id__in=('redhat:build-1', 'redhat:build-2'))),
            ('tests_triaged', models.KCIDBTest.objects.none()),
            ('tests_untriaged', models.KCIDBTest.objects.filter(id__in=('redhat:test-1', 'redhat:test-2'))),
            # Revision is not triaged.
            ('is_triaged', False),
            ('is_missing_triage', True),
        ]
        self._test_rev(cases)

    def test_partially_triaged(self):
        """Test some objects were triaged and some others not."""
        # Add some issues to some builds and tests.
        issue = models.Issue.objects.create(
            kind=models.IssueKind.objects.create(description="fail 1", tag="1"),
            description='foo bar',
            ticket_url='http://some.url',
        )
        models.KCIDBRevision.objects.get(id='decd6167bf4f6bec1284006d0522381b44660df3').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-1').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-1').issues.add(issue)

        # We now have both triaged and untriaged jobs.
        cases = [
            ('stats_revision_triaged', True),
            ('stats_revision_untriaged', False),
            ('stats_builds_triaged', True),
            ('stats_builds_untriaged', True),
            ('stats_tests_triaged', True),
            ('stats_tests_untriaged', True),
            # Some issues, missing triage.
            ('has_objects_missing_triage', True),
            ('has_objects_with_issues', True),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_triaged', models.KCIDBBuild.objects.filter(id='redhat:build-1')),
            ('builds_untriaged', models.KCIDBBuild.objects.filter(id='redhat:build-2')),
            ('tests_triaged', models.KCIDBTest.objects.filter(id='redhat:test-1')),
            ('tests_untriaged', models.KCIDBTest.objects.filter(id='redhat:test-2')),
            # Revision is triaged.
            ('is_triaged', True),
            ('is_missing_triage', False),
        ]
        self._test_rev(cases)

    def test_fully_triaged(self):
        """Test all objects were triaged."""
        # Add issue to all failures.
        issue = models.Issue.objects.create(
            kind=models.IssueKind.objects.create(description="fail 1", tag="1"),
            description='foo bar',
            ticket_url='http://some.url',
        )
        models.KCIDBRevision.objects.get(id='decd6167bf4f6bec1284006d0522381b44660df3').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-1').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-2').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-1').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-2').issues.add(issue)

        # We now have all issues triaged.
        cases = [
            ('stats_builds_triaged', True),
            ('stats_builds_untriaged', False),
            ('stats_tests_triaged', True),
            ('stats_tests_untriaged', False),
            # No failures missing triage.
            ('has_objects_missing_triage', False),
            ('has_objects_with_issues', True),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_untriaged', models.KCIDBBuild.objects.none()),
            ('builds_triaged', models.KCIDBBuild.objects.filter(id__in=('redhat:build-1', 'redhat:build-2'))),
            ('tests_untriaged', models.KCIDBTest.objects.none()),
            ('tests_triaged', models.KCIDBTest.objects.filter(id__in=('redhat:test-1', 'redhat:test-2')))
        ]
        self._test_rev(cases)


class TestKCIDBProperties(utils.TestCase):
    """Test objects properties."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_revision_web_url(self):
        """Test KCIDBRevision's web_url property."""
        revision = models.KCIDBRevision.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/revisions/{revision.iid}', revision.web_url)

    def test_build_web_url(self):
        """Test KCIDBBuild's web_url property."""
        build = models.KCIDBBuild.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/builds/{build.iid}', build.web_url)

    def test_test_web_url(self):
        """Test KCIDBTest's web_url property."""
        test = models.KCIDBTest.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/tests/{test.iid}', test.web_url)
