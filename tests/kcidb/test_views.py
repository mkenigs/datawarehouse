"""Test the views module."""
from datawarehouse import models
from tests import utils


class TestKCIDBViewsAnonymous(utils.KCIDBTestCase):
    """Test KCIDB frontend views."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_authorization.yaml',
    ]
    anonymous = True
    groups = []

    def test_revision_view(self):
        """Test revisions view."""
        self._ensure_test_conditions('read')
        authorized_revisions = models.KCIDBRevision.objects.filter(
            tree__id__in=self.trees_authorized['read']
        )

        for revision in models.KCIDBRevision.objects.all():
            response = self.client.get(f'/kcidb/revisions/{revision.iid}')

            if revision not in authorized_revisions:
                self.assertEqual(404, response.status_code)
                continue

            all_issues_affected = set(
                list(models.Issue.objects.filter(kcidbrevision=revision).values_list('id', flat=True)) +
                list(models.Issue.objects.filter(kcidbbuild__revision=revision).values_list('id', flat=True)) +
                list(models.Issue.objects.filter(kcidbtest__build__revision=revision).values_list('id', flat=True))
            )
            self.assertContextEqual(
                response.context,
                {
                    'issue_occurrences': [
                        {
                            'issue': issue,
                            'revisions': models.KCIDBRevision.objects.filter(iid=revision.iid, issues=issue),
                            'builds': models.KCIDBBuild.objects.filter(revision=revision, issues=issue),
                            'tests': models.KCIDBTest.objects.filter(build__revision=revision, issues=issue),
                        } for issue in models.Issue.objects.filter(id__in=all_issues_affected)
                    ],
                    'issues': models.Issue.objects.filter(resolved=False).order_by('-id'),
                    'tests': models.KCIDBTest.objects.filter(build__revision=revision),
                    'tests_failed': (
                        models.KCIDBTest.objects.filter(build__revision=revision)
                        .exclude(status=models.ResultEnum.PASS)
                    ),
                    'builds': models.KCIDBBuild.objects.filter(revision=revision),
                    'builds_failed': models.KCIDBBuild.objects.filter(revision=revision).exclude(valid=True),
                    'revision': revision,
                    'revisions_failed': [revision] if not revision.valid else [],
                },
            )

    def test_build_view(self):
        """Test build view."""
        self._ensure_test_conditions('read')
        authorized_builds = models.KCIDBBuild.objects.filter(
            revision__tree__id__in=self.trees_authorized['read']
        )

        for build in models.KCIDBBuild.objects.all():
            response = self.client.get(f'/kcidb/builds/{build.iid}')

            if build not in authorized_builds:
                self.assertEqual(404, response.status_code)
                continue

            all_issues_affected = set(
                list(models.Issue.objects.filter(kcidbbuild=build).values_list('id', flat=True)) +
                list(models.Issue.objects.filter(kcidbtest__build=build).values_list('id', flat=True))
            )
            self.assertContextEqual(
                response.context,
                {
                    'issue_occurrences': [
                        {
                            'issue': issue,
                            'builds': models.KCIDBBuild.objects.filter(iid=build.iid, issues=issue),
                            'tests': models.KCIDBTest.objects.filter(build=build, issues=issue),
                        } for issue in models.Issue.objects.filter(id__in=all_issues_affected)
                    ],
                    'issues': models.Issue.objects.filter(resolved=False).order_by('-id'),
                    'tests': models.KCIDBTest.objects.filter(build=build),
                    'tests_failed': (
                        models.KCIDBTest.objects.filter(build=build).exclude(status=models.ResultEnum.PASS)
                    ),
                    'build': build,
                    'builds_failed': [build] if not build.valid else [],
                },
            )

    def test_test_view(self):
        """Test Test view."""
        self._ensure_test_conditions('read')
        authorized_tests = models.KCIDBTest.objects.filter(
            build__revision__tree__in=self.trees_authorized['read']
        )

        for test in models.KCIDBTest.objects.all():
            response = self.client.get(f'/kcidb/tests/{test.iid}')

            if test not in authorized_tests:
                self.assertEqual(404, response.status_code)
                continue

            all_issues_affected = set(
                list(models.Issue.objects.filter(kcidbtest=test).values_list('id', flat=True))
            )
            self.assertContextEqual(
                response.context,
                {
                    'issue_occurrences': [
                        {
                            'issue': issue,
                            'tests': models.KCIDBTest.objects.filter(iid=test.iid, issues=issue),
                        } for issue in models.Issue.objects.filter(id__in=all_issues_affected)
                    ],
                    'issues': models.Issue.objects.filter(resolved=False).order_by('-id'),
                    'test': test,
                    'tests_failed': [test] if not test.status or test.status.name != 'PASS' else [],
                },
            )

    def test_create_issues_revision(self):
        """Test linking an issue to a revision."""
        self._ensure_test_conditions('write')
        authorized_revisions = models.KCIDBRevision.objects.filter(
            tree__id__in=self.trees_authorized['write']
        )

        issue = models.Issue.objects.first()

        for revision in models.KCIDBRevision.objects.all():
            revision.issues.clear()

            authorized = revision in authorized_revisions and not self.anonymous
            response_code = 302 if authorized else 404

            self.assert_authenticated_post(
                response_code,
                'change_kcidbrevision',
                '/kcidb/issues/occurrences',
                {
                    'issue_id': issue.id,
                    'revision_iids': [
                        revision.iid
                    ],
                },
                user=self.user
            )

            if not authorized:
                continue

            self.assertEqual(
                issue,
                revision.issues.get()
            )

    def test_create_issues_build(self):
        """Test linking an issue to builds."""
        self._ensure_test_conditions('write')
        authorized_builds = models.KCIDBBuild.objects.filter(
            revision__tree__id__in=self.trees_authorized['write']
        )

        issue = models.Issue.objects.first()

        for build in models.KCIDBBuild.objects.all():
            build.issues.clear()

            authorized = build in authorized_builds and not self.anonymous
            response_code = 302 if authorized else 404

            self.assert_authenticated_post(
                response_code,
                'change_kcidbbuild',
                '/kcidb/issues/occurrences',
                {
                    'issue_id': issue.id,
                    'build_iids': [
                        build.iid
                    ],
                },
                user=self.user
            )

            if not authorized:
                continue

            self.assertEqual(
                issue,
                build.issues.get()
            )

    def test_create_issues_test(self):
        """Test linking an issue to some tests."""
        self._ensure_test_conditions('write')
        authorized_tests = models.KCIDBTest.objects.filter(
            build__revision__tree__id__in=self.trees_authorized['write']
        )

        issue = models.Issue.objects.first()

        for test in models.KCIDBTest.objects.all():
            test.issues.clear()

            authorized = test in authorized_tests and not self.anonymous
            response_code = 302 if authorized else 404

            self.assert_authenticated_post(
                response_code,
                'change_kcidbtest',
                '/kcidb/issues/occurrences',
                {
                    'issue_id': issue.id,
                    'test_iids': [
                        test.iid
                    ],
                },
                user=self.user
            )

            if not authorized:
                continue

            self.assertEqual(
                issue,
                test.issues.get()
            )

    def test_create_issues_all(self):
        """
        Test linking an issue to revisions, builds and tests.

        At least doesn't have authorization to one of the revisions, so it should fail.
        """
        self._ensure_test_conditions('write')
        issue = models.Issue.objects.first()
        revisions = models.KCIDBRevision.objects.all()
        builds = models.KCIDBBuild.objects.all()
        tests = models.KCIDBTest.objects.all()

        self.assert_authenticated_post(
            404,
            ['change_kcidbrevision', 'change_kcidbbuild', 'change_kcidbtest'],
            '/kcidb/issues/occurrences',
            {
                'issue_id': issue.id,
                'revision_iids': list(revisions.values_list('iid', flat=True)),
                'build_iids': list(builds.values_list('iid', flat=True)),
                'test_iids': list(tests.values_list('iid', flat=True))
            },
            user=self.user
        )

    def test_create_issues_all_allowed(self):
        """
        Test linking an issue to revisions, builds and tests.

        All the submitted objects are allowed to this user.
        """
        self._ensure_test_conditions('write')
        issue = models.Issue.objects.first()
        revisions = models.KCIDBRevision.objects.filter(tree__id__in=self.trees_authorized['write'])
        builds = models.KCIDBBuild.objects.filter(revision__tree__id__in=self.trees_authorized['write'])
        tests = models.KCIDBTest.objects.filter(build__revision__tree__id__in=self.trees_authorized['write'])

        [revision.issues.clear() for revision in revisions]
        [build.issues.clear() for build in builds]
        [test.issues.clear() for test in tests]

        self.assert_authenticated_post(
            302,
            ['change_kcidbrevision', 'change_kcidbbuild', 'change_kcidbtest'],
            '/kcidb/issues/occurrences',
            {
                'issue_id': issue.id,
                'revision_iids': list(revisions.values_list('iid', flat=True)),
                'build_iids': list(builds.values_list('iid', flat=True)),
                'test_iids': list(tests.values_list('iid', flat=True))
            },
            user=self.user
        )

        if self.anonymous:
            return

        for obj in revisions:
            self.assertEqual(issue, obj.issues.get())
        for obj in builds:
            self.assertEqual(issue, obj.issues.get())
        for obj in tests:
            self.assertEqual(issue, obj.issues.get())

    def test_delete_issues_all(self):
        """
        Test deleting an issue from revisions, builds and tests.

        At least doesn't have authorization to one of the revisions, so it should fail.
        """
        self._ensure_test_conditions('write')
        issue = models.Issue.objects.first()
        revisions = models.KCIDBRevision.objects.all()
        builds = models.KCIDBBuild.objects.all()
        tests = models.KCIDBTest.objects.all()

        self.assert_authenticated_post(
            404,
            ['change_kcidbrevision', 'change_kcidbbuild', 'change_kcidbtest'],
            '/kcidb/issues/occurrences',
            {
                'action': 'remove',
                'issue_id': issue.id,
                'revision_iids': list(revisions.values_list('iid', flat=True)),
                'build_iids': list(builds.values_list('iid', flat=True)),
                'test_iids': list(tests.values_list('iid', flat=True))
            },
            user=self.user
        )

    def test_delete_issues_all_allowed(self):
        """
        Test deleting an issue from revisions, builds and tests.

        All the submitted objects are allowed to this user.
        """
        self._ensure_test_conditions('write')
        issue = models.Issue.objects.first()
        revisions = models.KCIDBRevision.objects.filter(tree__id__in=self.trees_authorized['write'])
        builds = models.KCIDBBuild.objects.filter(revision__tree__id__in=self.trees_authorized['write'])
        tests = models.KCIDBTest.objects.filter(build__revision__tree__id__in=self.trees_authorized['write'])

        [revision.issues.add(issue) for revision in revisions]
        [build.issues.add(issue) for build in builds]
        [test.issues.add(issue) for test in tests]

        self.assert_authenticated_post(
            302,
            ['change_kcidbrevision', 'change_kcidbbuild', 'change_kcidbtest'],
            '/kcidb/issues/occurrences',
            {
                'action': 'remove',
                'issue_id': issue.id,
                'revision_iids': list(revisions.values_list('iid', flat=True)),
                'build_iids': list(builds.values_list('iid', flat=True)),
                'test_iids': list(tests.values_list('iid', flat=True))
            },
            user=self.user
        )

        if self.anonymous:
            return

        for obj in revisions:
            self.assertFalse(obj.issues.filter(id=issue.id).exists())
        for obj in builds:
            self.assertFalse(obj.issues.filter(id=issue.id).exists())
        for obj in tests:
            self.assertFalse(obj.issues.filter(id=issue.id).exists())


class TestKCIDBViewsNoGroup(TestKCIDBViewsAnonymous):
    """TestKCIDBViews with a group with no groups assigned."""

    anonymous = False
    groups = []


class TestKCIDBViewsReadGroups(TestKCIDBViewsAnonymous):
    """TestKCIDBViews with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestKCIDBViewsWriteGroups(TestKCIDBViewsAnonymous):
    """TestKCIDBViews with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestKCIDBViewsAllGroups(TestKCIDBViewsAnonymous):
    """TestKCIDBViews with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestRevisionsListAnonymous(utils.KCIDBTestCase):
    """Test the revisions list view."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
    ]
    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_revisions = models.KCIDBRevision.objects.filter(tree__id__in=self.trees_authorized[method])
        no_auth_revisions = models.KCIDBRevision.objects.exclude(tree__id__in=self.trees_authorized[method])

        checks = [
            (auth_revisions, 'No authorized revisions'),
            (no_auth_revisions, 'No unauthorized revisions'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def setUp(self):
        """Set up data."""
        origin = models.KCIDBOrigin.objects.get(name='redhat')
        public_tree = models.GitTree.objects.get(name='tree_1')
        restricted_tree = models.GitTree.objects.get(name='tree_2')
        unavailable_tree = models.GitTree.objects.get(name='tree_3')

        models.KCIDBRevision.objects.bulk_create(
            [
                models.KCIDBRevision(
                    origin=origin,
                    id=f'public_{index}',
                    tree=public_tree,
                )
                for index in range(40)
            ]
        )
        models.KCIDBRevision.objects.bulk_create(
            [
                models.KCIDBRevision(
                    origin=origin,
                    id=f'restricted_{index}',
                    tree=restricted_tree,
                )
                for index in range(40)
            ]
        )
        models.KCIDBRevision.objects.bulk_create(
            [
                models.KCIDBRevision(
                    origin=origin,
                    id=f'unavailable_{index}',
                    tree=unavailable_tree,
                )
                for index in range(40)
            ]
        )

        super().setUp()

    def test_revision_list(self):
        """Test revisions list."""
        revisions = models.KCIDBRevision.objects.filter(tree__id__in=self.trees_authorized['read'])
        response = self.client.get('/kcidb/revisions')
        self.assertContextEqual(
            response.context,
            {
                'revisions': revisions[:30],
            },
        )

    def test_revision_list_page_2(self):
        """Test revisions list pagination."""
        revisions = models.KCIDBRevision.objects.filter(tree__id__in=self.trees_authorized['read'])
        response = self.client.get('/kcidb/revisions?page=2')
        self.assertContextEqual(
            response.context,
            {
                'revisions': revisions[30:60],
            },
        )

    def test_revision_list_page_invalid(self):
        """Test revisions list pagination with wrong page numbers."""
        revisions = models.KCIDBRevision.objects.filter(tree__id__in=self.trees_authorized['read'])
        for page in ('...', 'foobar', '3%3BSELECT%20sleep%2829%29%3B%20--', '0', '-1', '-1000'):
            response = self.client.get(f'/kcidb/revisions?page={page}')
            self.assertContextEqual(
                response.context,
                {
                    'revisions': revisions[:30],
                },
            )

    def test_revision_list_aggregated(self):
        """Test revisions list returns aggregated results."""
        response = self.client.get('/kcidb/revisions')
        self.assertTrue(
            hasattr(
                response.context['revisions'][0],
                'stats_tests_fail_count'
            )
        )

    def test_failures_filtered(self):
        """Test revisions list filtered."""
        models.KCIDBRevision.objects.all().delete()

        origin = models.KCIDBOrigin.objects.first()

        policy_permissive = models.Policy.objects.create(
            name='permissive', write_group=None, read_group=None
        )
        tree_1 = models.GitTree.objects.create(name='tree_1', policy=policy_permissive)
        tree_2 = models.GitTree.objects.create(name='tree_2', policy=policy_permissive)

        rev_1 = models.KCIDBRevision.objects.create(id='1', origin=origin, tree=tree_1)
        rev_2 = models.KCIDBRevision.objects.create(id='2', origin=origin, tree=tree_2)

        # Filter tree_1
        response = self.client.get('/kcidb/revisions?filter_gittrees=tree_1')
        self.assertContextEqual(response.context, {'revisions': [rev_1]})

        # Filter tree_2
        response = self.client.get('/kcidb/revisions?filter_gittrees=tree_2')
        self.assertContextEqual(response.context, {'revisions': [rev_2]})


class TestRevisionsListNoGroups(TestRevisionsListAnonymous):
    """TestRevisionList with no groups."""

    anonymous = False
    groups = []


class TestRevisionsListReadGroups(TestRevisionsListAnonymous):
    """TestRevisionList with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestRevisionsListWriteGroups(TestRevisionsListAnonymous):
    """TestRevisionList with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestRevisionsListAllGroups(TestRevisionsListAnonymous):
    """TestRevisionList with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestRevisionsFailuresAnonymous(utils.KCIDBTestCase):
    """Test the revisions failures list view."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_failed_revisions.yaml'
    ]
    anonymous = True
    groups = []

    def test_failures_all(self):
        """Test failures list. All."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/all')
        self.assertContextEqual(
            response.context,
            {
                'revisions': (
                    models.KCIDBRevision.objects
                    .filter(tree__id__in=self.trees_authorized['read'])
                    .exclude(iid__in=(5, 10, 15))  # These ones do not have failures
                )
            },
        )

    def test_failures_revisions(self):
        """Test failures list. Revisions."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/revision')
        self.assertContextEqual(
            response.context,
            {
                'revisions': (
                    models.KCIDBRevision.objects
                    .filter(tree__id__in=self.trees_authorized['read'])
                    .filter(valid=False)
                )
            },
        )

    def test_failures_builds(self):
        """Test failures list. Builds."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/build')
        self.assertContextEqual(
            response.context,
            {
                'revisions': (
                    models.KCIDBRevision.objects
                    .filter(tree__id__in=self.trees_authorized['read'])
                    .filter(kcidbbuild__valid=False)
                )
            },
        )

    def test_failures_tests(self):
        """Test failures list. Tests."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/test')
        print(models.KCIDBTest.objects.filter(build__revision__id='restricted_revision_4'))
        self.assertContextEqual(
            response.context,
            {
                'revisions': (
                    models.KCIDBRevision.objects
                    .filter(tree__id__in=self.trees_authorized['read'])
                    .filter(kcidbbuild__kcidbtest__status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES)
                )
            },
        )

    def test_pagination(self):
        """Test list pagination."""
        self._ensure_test_conditions('read')
        # Remove previous objects
        models.KCIDBRevision.objects.all().delete()

        origin = models.KCIDBOrigin.objects.first()
        public_tree = models.GitTree.objects.get(name='tree_1')
        models.KCIDBRevision.objects.bulk_create([
            models.KCIDBRevision(
                id=f'public_revision_{index}',
                origin=origin,
                valid=False,
                tree=public_tree,
            ) for index in range(40)
        ])

        response = self.client.get('/kcidb/failures/revision?page=2')

        self.assertContextEqual(
            response.context,
            {
                'revisions': models.KCIDBRevision.objects.all()[30:],
            },
        )

    def test_list_aggregated(self):
        """Test revisions list returns aggregated results."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/all')
        self.assertTrue(
            hasattr(
                response.context['revisions'][0],
                'stats_tests_fail_count'
            )
        )

    def test_unknown(self):
        """Test revisions list returns aggregated results."""
        response = self.client.get('/kcidb/failures/foobar')
        self.assertEqual(400, response.status_code)
        self.assertEqual(b'Not sure what foobar is.', response.content)

    def test_failures_filtered(self):
        """Test failures list. Filtered."""
        policy_permissive = models.Policy.objects.create(
            name='permissive', write_group=None, read_group=None
        )
        models.GitTree.objects.filter(name='tree_2').update(policy=policy_permissive)
        response = self.client.get('/kcidb/failures/all?filter_gittrees=tree_2')
        self.assertTrue(models.KCIDBRevision.objects.filter(tree__name='tree_2').exists())
        self.assertContextEqual(
            response.context,
            {
                'revisions': (
                    models.KCIDBRevision.objects.filter(tree__name='tree_2')
                    .exclude(id='restricted_revision_5')  # Successful
                )
            },
        )


class TestRevisionsFailuresNoGroups(TestRevisionsFailuresAnonymous):
    """TestRevisionsFailures with no groups."""

    anonymous = False
    groups = []


class TestRevisionsFailuresReadGroup(TestRevisionsFailuresAnonymous):
    """TestRevisionsFailures with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestRevisionsFailuresWriteGroup(TestRevisionsFailuresAnonymous):
    """TestRevisionsFailures with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestRevisionsFailuresAllGroups(TestRevisionsFailuresAnonymous):
    """TestRevisionsFailures with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestRevisionsSearchAnonymous(utils.KCIDBTestCase):
    """Test the revisions search view."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/pipeline_kcidb.yaml',
    ]

    anonymous = False
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_revisions = models.KCIDBRevision.objects.filter(tree__id__in=self.trees_authorized[method])
        no_auth_revisions = models.KCIDBRevision.objects.exclude(tree__id__in=self.trees_authorized[method])
        auth_pipelines = models.Pipeline.objects.filter(gittree__id__in=self.trees_authorized[method])
        no_auth_pipelines = models.Pipeline.objects.exclude(gittree__id__in=self.trees_authorized[method])

        checks = [
            (auth_revisions, 'No authorized revisions'),
            (no_auth_revisions, 'No unauthorized revisions'),
            (auth_pipelines, 'No authorized pipelines'),
            (no_auth_pipelines, 'No unauthorized pipelines'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_search_empty(self):
        """Test search view with empty query."""
        self._ensure_test_conditions('read')
        response = self.client.get('/search')
        self.assertContextEqual(response.context, {})

        response = self.client.get('/search?q=')
        self.assertContextEqual(response.context, {})

    def test_search_not_found(self):
        """Test search view with empty query."""
        self._ensure_test_conditions('read')
        response = self.client.get('/search?q=foobar')
        self.assertContextEqual(response.context, {})

    def test_search_by_id(self):
        """Test search view with id."""
        self._ensure_test_conditions('read')
        authorized_revisions = models.KCIDBRevision.objects.filter(
            tree__id__in=self.trees_authorized['read']
        )

        for revision in models.KCIDBRevision.objects.all():
            response = self.client.get(f'/search?q={revision.id}')
            self.assertEqual(200, response.status_code)

            if revision not in authorized_revisions:
                expected = {'revisions': []}
            else:
                expected = {'revisions': [revision]}

            self.assertContextEqual(response.context, expected)

    def test_search_by_iid(self):
        """Test search view with iid."""
        self._ensure_test_conditions('read')
        authorized_revisions = models.KCIDBRevision.objects.filter(
            tree__id__in=self.trees_authorized['read']
        )

        for revision in models.KCIDBRevision.objects.all():
            response = self.client.get(f'/search?q={revision.iid}')
            self.assertEqual(200, response.status_code)

            if revision not in authorized_revisions:
                expected = {'revisions': []}
            else:
                expected = {'revisions': [revision]}

            self.assertContextEqual(response.context, expected)

    def test_search_by_pipeline_id(self):
        """Test search view with pipeline_id."""
        self._ensure_test_conditions('read')
        authorized_revisions = models.KCIDBRevision.objects.filter(
            tree__id__in=self.trees_authorized['read']
        )

        for revision in models.KCIDBRevision.objects.all():
            response = self.client.get(f'/search?q={revision.gitlabjob_set.first().pipeline.pipeline_id}')
            self.assertEqual(200, response.status_code)

            if revision not in authorized_revisions:
                expected = {'revisions': []}
            else:
                expected = {'revisions': [revision]}

            self.assertContextEqual(response.context, expected)

    def test_search_by_job_id(self):
        """Test search view with job id."""
        self._ensure_test_conditions('read')
        authorized_revisions = models.KCIDBRevision.objects.filter(
            tree__id__in=self.trees_authorized['read']
        )

        for revision in models.KCIDBRevision.objects.all():
            response = self.client.get(f'/search?q={revision.gitlabjob_set.first().job_id}')
            self.assertEqual(200, response.status_code)

            if revision not in authorized_revisions:
                expected = {'revisions': []}
            else:
                expected = {'revisions': [revision]}

            self.assertContextEqual(response.context, expected)


class TestRevisionsSearchReadGroup(TestRevisionsSearchAnonymous):
    """Test the revisions search view. Read group."""

    anonymous = False
    groups = ['group_a']


class TestRevisionsSearchWriteGroup(TestRevisionsSearchAnonymous):
    """Test the revisions search view. Write group."""

    anonymous = False
    groups = ['group_b']


class TestRevisionsSearchAllGroups(TestRevisionsSearchAnonymous):
    """Test the revisions search view. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']
