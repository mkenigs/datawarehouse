"""Test kcidb serializers."""
import kcidb

from datawarehouse import models
from datawarehouse.api.kcidb import serializers
from tests import utils


class TestSerializers(utils.TestCase):
    # pylint: disable=too-many-instance-attributes
    """Test KCIDB-schemed serializers."""

    schema = kcidb.io.schema.v3.VERSION
    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_complete.yaml'
    ]

    def test_revision(self):
        """Test KCIDBRevision object seralization is valid."""
        data = {
            'version': {
                'major': self.schema.major,
                'minor': self.schema.minor,
            },
            'revisions': [
                serializers.KCIDBRevisionSerializer(
                    models.KCIDBRevision.objects.get(iid=1)
                ).data
            ]
        }

        self.schema.validate_exactly(data)

    def test_build(self):
        """Test KCIDBBuild object seralization is valid."""
        data = {
            'version': {
                'major': self.schema.major,
                'minor': self.schema.minor,
            },
            'builds': [
                serializers.KCIDBBuildSerializer(
                    models.KCIDBBuild.objects.get(iid=1)
                ).data
            ]
        }

        self.schema.validate_exactly(data)

    def test_test(self):
        """Test KCIDBTest object seralization is valid."""
        data = {
            'version': {
                'major': self.schema.major,
                'minor': self.schema.minor,
            },
            'tests': [
                serializers.KCIDBTestSerializer(
                    models.KCIDBTest.objects.get(iid=1)
                ).data
            ]
        }

        self.schema.validate_exactly(data)
