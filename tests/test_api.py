"""Test the views module."""
import json

from django.contrib.auth.models import Permission  # pylint: disable=imported-auth-user
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from datawarehouse import models
from datawarehouse import serializers
from tests import utils


class TestAPITokenAuthentication(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Smoke test to keep token auth working."""

    revision_id = 'a17c8f36b72cc5422a1897ff057eddd2b62ebac2'

    def setUp(self):
        """Set Up."""
        models.KCIDBRevision.objects.create(
            origin=models.KCIDBOrigin.objects.create(name='redhat'),
            id=self.revision_id,
            tree=models.GitTree.objects.create(
                name='test_tree',
                policy=models.Policy.objects.create(
                    name='test_policy',
                    read_group=None,
                    write_group=None,
                )
            )
        )
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)

        permissions = Permission.objects.filter(
            codename__in=['add_kcidbrevision', 'change_kcidbrevision', 'delete_kcidbrevision']
        )

        test_user = User.objects.create(username='test', email='test@test.com')
        test_user.user_permissions.set(permissions)
        token = Token.objects.create(user=test_user)

        self.api_client = APIClient()
        self.api_client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_post(self):
        """Test POST request."""
        issue = models.Issue.objects.first()

        response = self.api_client.post(
            f'/api/1/kcidb/revisions/{self.revision_id}/issues',
            json.dumps({'issue_id': issue.id}), content_type="application/json")
        self.assertEqual(201, response.status_code)

    def test_delete(self):
        """Test DELETE request."""
        issue = models.Issue.objects.first()
        models.KCIDBRevision.objects.get(id=self.revision_id).issues.add(
            issue
        )

        response = self.api_client.delete(
            f'/api/1/kcidb/revisions/{self.revision_id}/issues/{issue.id}',
            content_type="application/json")
        self.assertEqual(204, response.status_code)


class TestTestAPI(utils.TestCase):
    """Unit tests for the Test API endpoint."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/fixtures/base_simple.yaml'
    ]

    def test_get_test(self):
        """Test get all tests."""
        response = self.client.get('/api/1/test')
        self.assertEqual(
            response.json()['results'],
            serializers.TestSerializer(models.Test.objects.all(), many=True).data
        )

    def test_get_single_test(self):
        """Test get single tests."""
        response = self.client.get('/api/1/test/1')
        self.assertEqual(
            response.json(),
            serializers.TestSerializer(models.Test.objects.get(id=1)).data
        )

    def test_get_single_test_404(self):
        """Test get single tests. It doesn't exist."""
        response = self.client.get('/api/1/test/1234')
        self.assertEqual(404, response.status_code)


class TestIssueRegexAPIAnonymous(utils.KCIDBTestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Unit tests for the IssueRegex API endpoint."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
        'tests/fixtures/issue_regexes.yaml'
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_issue_regexes = models.IssueRegex.objects.filter(issue__id__in=self.issues_authorized[method])
        no_auth_issue_regexes = models.IssueRegex.objects.exclude(issue__id__in=self.issues_authorized[method])

        checks = [
            (auth_issue_regexes, 'No authorized issue_regexes'),
            (no_auth_issue_regexes, 'No unauthorized issue_regexes'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_get_issue_regex(self):
        """Test get issue regex list."""
        self._ensure_test_conditions('read')
        authorized_issue_regexes = models.IssueRegex.objects.filter(
            issue__id__in=self.issues_authorized['read']
        )

        response = self.client.get('/api/1/issue/regex')

        self.assertEqual(
            response.json()['results'],
            serializers.IssueRegexSerializer(authorized_issue_regexes, many=True).data
        )


class TestIssueRegexAPINoGroup(TestIssueRegexAPIAnonymous):
    """Unit tests for the IssueRegex API endpoint. No group."""

    anonymous = False
    groups = []


class TestIssueRegexAPIReadGroup(TestIssueRegexAPIAnonymous):
    """Unit tests for the IssueRegex API endpoint. Read group."""

    anonymous = False
    groups = ['group_a']


class TestIssueRegexAPIWriteGroup(TestIssueRegexAPIAnonymous):
    """Unit tests for the IssueRegex API endpoint. Write group."""

    anonymous = False
    groups = ['group_b']


class TestIssueRegexAPIAllGroups(TestIssueRegexAPIAnonymous):
    """Unit tests for the IssueRegex API endpoint. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestIssueAPIAnonymous(utils.KCIDBTestCase):
    """Tests for the Issue endpoints."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
    ]
    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_issues = models.Issue.objects.filter(id__in=self.issues_authorized[method])
        no_auth_issues = models.Issue.objects.exclude(id__in=self.issues_authorized[method])

        checks = [
            (auth_issues, 'No authorized issues'),
            (no_auth_issues, 'No unauthorized issues'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_list_issues(self):
        """Test list issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        response = self.client.get('/api/1/issue')

        self.assertEqual(
            response.json()['results'],
            serializers.IssueSerializer(authorized_issues, many=True).data,
        )

    def test_list_issues_resolved(self):
        """Test list resolved issues."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        response = self.client.get('/api/1/issue?resolved=True')

        self.assertEqual(
            response.json()['results'],
            serializers.IssueSerializer(authorized_issues.filter(resolved=True), many=True).data,
        )

    def test_get_issue(self):
        """Test get issue."""
        self._ensure_test_conditions('read')
        authorized_issues = models.Issue.objects.filter(
            id__in=self.issues_authorized['read']
        )

        for issue in models.Issue.objects.all():
            response = self.client.get(f'/api/1/issue/{issue.id}')

            if issue not in authorized_issues:
                self.assertEqual(404, response.status_code)
                continue

            self.assertEqual(
                response.json(),
                serializers.IssueSerializer(issue).data,
            )


class TestIssueAPINoGroups(TestIssueAPIAnonymous):
    """Tests for the Issue endpoints with no groups."""

    anonymous = False
    groups = []


class TestIssueAPIReadGroup(TestIssueAPIAnonymous):
    """Tests for the Issue endpoints with read groups."""

    anonymous = False
    groups = ['group_a']


class TestIssueAPIWriteGroup(TestIssueAPIAnonymous):
    """Tests for the Issue endpoints with write groups."""

    anonymous = False
    groups = ['group_b']


class TestIssueAPIAllGroups(TestIssueAPIAnonymous):
    """Tests for the Issue endpoints with read and write groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class PipelineViewsTestCaseAnonymous(utils.KCIDBTestCase):
    """Unittest for the Pipeline API endpoint. Anonymous."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/pipelines.yaml'
    ]

    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_pipelines = models.Pipeline.objects.filter(gittree__id__in=self.trees_authorized[method])
        no_auth_pipelines = models.Pipeline.objects.exclude(gittree__id__in=self.trees_authorized[method])

        checks = [
            (auth_pipelines, 'No authorized pipelines'),
            (no_auth_pipelines, 'No unauthorized pipelines'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_pipeline_get_json(self):
        """Test get_pipeline json."""
        self._ensure_test_conditions('read')
        authorized_pipelines = models.Pipeline.objects.filter(
            gittree__id__in=self.trees_authorized['read']
        )

        for pipeline in models.Pipeline.objects.all():
            response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}')
            if pipeline not in authorized_pipelines:
                self.assertEqual(404, response.status_code)
                continue

            self.assertEqual(200, response.status_code)

            self.assertEqual(
                serializers.PipelineSerializer(pipeline).data,
                response.json()
            )


class PipelineViewsTestCaseNoGroup(PipelineViewsTestCaseAnonymous):
    """Unittest for the Pipeline API endpoint. No groups."""

    anonymous = False
    groups = []


class PipelineViewsTestCaseReadGroups(PipelineViewsTestCaseAnonymous):
    """Unittest for the Pipeline API endpoint. Read groups."""

    anonymous = False
    groups = ['group_a']


class PipelineViewsTestCaseWriteGroups(PipelineViewsTestCaseAnonymous):
    """Unittest for the Pipeline API endpoint. Write groups."""

    anonymous = False
    groups = ['group_b']


class PipelineViewsTestCaseAllGroups(PipelineViewsTestCaseAnonymous):
    """Unittest for the Pipeline API endpoint. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']
