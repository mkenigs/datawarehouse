"""Urls file."""
from django.urls import include
from django.urls import path

from . import views

urlpatterns = [
    path('1/pipeline/<int:pipeline_id>', views.Pipeline.as_view()),
    path('1/issue/<int:issue_id>', views.IssueGet.as_view()),
    path('1/issue', views.IssueList.as_view()),
    path('1/issue/regex', views.IssueRegex.as_view()),
    path('1/test/<int:test_id>', views.TestSingle.as_view()),
    path('1/test', views.TestList.as_view()),
    path('1/kcidb/', include('datawarehouse.api.kcidb.urls')),
]
