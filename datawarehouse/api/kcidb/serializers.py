# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# pylint: disable=no-self-use
"""Serializers."""
from rest_framework import serializers

from datawarehouse import models

VERSION = {'major': 3, 'minor': 0}


class NoEmptyFieldSerializer(serializers.ModelSerializer):
    """Don't serialize empty fields."""

    @staticmethod
    def _value_is_empty(value):
        """Return True if the value is empty."""
        return (
            value is None or
            value == {} or
            value == []
        )

    def to_representation(self, instance):
        """Filter out empty values from the representation."""
        result = super().to_representation(instance)
        return {
            key: value for key, value in result.items()
            if not self._value_is_empty(value)
        }


class KCIDBPatchMBOXSerializer(serializers.ModelSerializer):
    """Serializer for Patch KCIDB styled."""

    name = serializers.CharField(source='subject', read_only=True)

    class Meta:
        """Metadata."""

        model = models.Patch
        fields = ('name', 'url')


class KCIDBFileSerializer(serializers.ModelSerializer):
    """Serializer for File KCIDB styled."""

    class Meta:
        """Metadata."""

        model = models.Artifact
        fields = ('url', 'name')


class KCIDBRevisionSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBRevision."""

    origin = serializers.CharField(source='origin.name')
    tree_name = serializers.CharField(source='tree.name', read_only=True)
    patch_mboxes = KCIDBPatchMBOXSerializer(many=True, source='patches')
    log_url = serializers.CharField(source='log.url', read_only=True)
    contacts = serializers.SerializerMethodField()
    misc = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.KCIDBRevision
        fields = ('id', 'origin', 'tree_name',
                  'git_repository_url', 'git_repository_branch',
                  'git_commit_hash', 'git_commit_name',
                  'patch_mboxes', 'message_id', 'description',
                  'publishing_time', 'discovery_time', 'log_url',
                  'contacts', 'valid', 'misc',
                  )

    def get_contacts(self, revision):
        """Return contacts as a flat list of emails."""
        return list(revision.contacts.values_list('email', flat=True))

    def get_misc(self, revision):
        """Return misc field."""
        return {
            'kcidb': {'version': VERSION},
            'iid': revision.iid,
            'is_public': revision.is_public,
            'nvr': revision.kernel_version,
        }


class KCIDBBuildSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBBuild."""

    revision_id = serializers.CharField(source='revision.id')
    origin = serializers.CharField(source='origin.name')
    architecture = serializers.SerializerMethodField()
    log_url = serializers.CharField(source='log.url', read_only=True)
    compiler = serializers.CharField(source='compiler.name', read_only=True)
    input_files = KCIDBFileSerializer(many=True)
    output_files = KCIDBFileSerializer(many=True)
    misc = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.KCIDBBuild
        fields = ('revision_id', 'id', 'origin', 'description',
                  'start_time', 'duration', 'architecture',
                  'command', 'compiler', 'input_files', 'output_files', 'config_name',
                  'config_url', 'log_url', 'valid', 'misc',
                  )

    def get_architecture(self, obj):
        """Return architecture name."""
        return obj.get_architecture_display()

    def get_misc(self, build):
        """Return misc field."""
        return {
            'kcidb': {'version': VERSION},
            'iid': build.iid,
            'is_public': build.is_public,
        }


class EnvironmentSerializer(serializers.ModelSerializer):
    """Serializer for Environment (BeakerResource)."""

    description = serializers.CharField(source='fqdn')

    class Meta:
        """Metadata."""

        model = models.BeakerResource
        fields = ('description', )


class KCIDBTestSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBTest."""

    build_id = serializers.CharField(source='build.id')
    origin = serializers.CharField(source='origin.name')
    environment = EnvironmentSerializer()
    description = serializers.CharField(source='test.name', read_only=True)
    path = serializers.CharField(source='test.universal_id', read_only=True)
    status = serializers.SerializerMethodField()
    output_files = KCIDBFileSerializer(many=True)
    misc = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.KCIDBTest
        fields = ('build_id', 'id', 'origin', 'environment',
                  'path', 'description', 'status', 'waived',
                  'start_time', 'duration', 'output_files', 'misc',
                  )

    def get_status(self, obj):
        """Return status name."""
        return obj.get_status_display()

    def get_misc(self, test):
        """Return misc field."""
        misc = {
            'kcidb': {'version': VERSION},
            'iid': test.iid,
            'is_public': test.is_public,
        }

        beaker_task = test.beakertask_set.last()
        if beaker_task:
            misc.update({
                'beaker': {
                    'task_id': beaker_task.task_id,
                    'recipe_id': beaker_task.recipe_id,
                }
            })

        return misc
