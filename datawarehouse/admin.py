# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Admin file."""
from django.contrib import admin

from . import models

admin.site.register(models.Project)
admin.site.register(models.GitTree)
admin.site.register(models.Policy)
admin.site.register(models.Pipeline)
admin.site.register(models.TriggerVariable)
admin.site.register(models.Patch)
admin.site.register(models.TestMaintainer)
admin.site.register(models.Test)
admin.site.register(models.BeakerResource)
admin.site.register(models.Artifact)
admin.site.register(models.Issue)
admin.site.register(models.IssueKind)
admin.site.register(models.IssueRegex)
admin.site.register(models.Report)
admin.site.register(models.Recipient)
