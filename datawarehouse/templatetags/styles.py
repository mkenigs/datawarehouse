"""Style templatetags."""
from django.template import Library

from datawarehouse import styles

register = Library()


@register.filter
def get_style(result):
    """Get style format for a given result."""
    if result is True:
        result = 'P'
    elif result is False:
        result = 'F'

    return styles.get_style(result)
