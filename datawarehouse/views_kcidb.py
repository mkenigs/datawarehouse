"""KCIDB Views."""
from cki_lib.logger import get_logger
from django.db.models import Q
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseForbidden
from django.http import HttpResponseNotFound
from django.http import HttpResponseRedirect
from django.template import loader

from . import authorization
from . import models
from . import pagination
from . import utils

LOGGER = get_logger(__name__)


def revisions_list(request):
    """Get list of revisions."""
    template = loader.get_template('web/kcidb/revisions.html')
    page = request.GET.get('page')

    revisions = models.KCIDBRevision.objects.filter_authorized(request)
    revisions, filters = utils.filter_revisions_view(request, revisions)

    paginator = pagination.EndlessPaginator(
        revisions.values_list('iid', flat=True),
        30
    )
    revision_iids = paginator.get_page(page)

    revisions = (
        models.KCIDBRevision.objects
        .aggregated()
        .filter(iid__in=revision_iids)
        .prefetch_related(
            'tree',
            'gitlabjob_set',
            'gitlabjob_set__pipeline',
        )
    )

    context = {
        'revisions': revisions,
        'paginator': revision_iids,
        # Filter parameters
        'gittrees': models.GitTree.objects.all().order_by('name'),
        'filters': filters,
    }

    return HttpResponse(template.render(context, request))


def revisions_list_by_failure(request, stage):
    """Show failed revisions classified by stage."""
    template = loader.get_template('web/kcidb/revisions_failures.html')
    page = request.GET.get('page')

    revisions = models.KCIDBRevision.objects.filter_authorized(request)

    objects = {
        'revision': revisions.filter(valid=False),
        'build': revisions.filter(kcidbbuild__valid=False),
        'test': revisions.filter(
            # Filter failed tests and exclude the waived ones.
            # .exclude() generated a *really* slow query. See cki-project/datawarehouse!258
            # waived__in=(False, None) doesn't handle NULL. See https://code.djangoproject.com/ticket/13579
            #
            # status in UNSUCCESSFUL_STATUSES & (waived==False | waived is NULL)
            Q(kcidbbuild__kcidbtest__status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES) &
            (
                Q(kcidbbuild__kcidbtest__waived=False) |
                Q(kcidbbuild__kcidbtest__waived__isnull=True)
            )
        )
    }

    if stage not in ['all'] + list(objects):
        return HttpResponseBadRequest(f'Not sure what {stage} is.')

    stages = list(objects) if stage == 'all' else [stage]

    # Get a list of iids of the revisions with failures
    revision_iids = []
    for stage_name in stages:
        revisions, filters = utils.filter_revisions_view(request, objects[stage_name])
        revision_iids.extend(
            revisions.values_list('iid', flat=True)
        )

    # Sort ids descendingly and remove duplicates
    revision_iids = sorted(list(set(revision_iids)), reverse=True)

    paginator = pagination.EndlessPaginator(revision_iids, 30)
    revision_iids_page = paginator.get_page(page)

    revisions = (
        models.KCIDBRevision.objects
        .aggregated()
        .filter(iid__in=revision_iids_page)
        .prefetch_related(
            'tree',
            'gitlabjob_set',
            'gitlabjob_set__pipeline',
        )
    )

    context = {
        'paginator': revision_iids_page,
        'revisions': revisions,
        'stage_filter': stage,
        'stages': list(objects),
        # Filter parameters
        'gittrees': models.GitTree.objects.all().order_by('name'),
        'filters': filters,
    }

    return HttpResponse(template.render(context, request))


def revisions_get(request, revision_iid):
    """Get a single revision."""
    template = loader.get_template('web/kcidb/revision.html')

    tree = models.KCIDBRevision.objects.get(iid=revision_iid).tree
    if not tree.is_read_authorized(request):
        return HttpResponseNotFound()

    revision = (
        models.KCIDBRevision.objects.aggregated()
        .select_related(
            'log',
            'origin',
            'tree',
        )
        .prefetch_related(
            'patches',
            'kcidbbuild_set',
            'kcidbbuild_set__kcidbtest_set',
            'kcidbbuild_set__kcidbtest_set__test',
        )
        .get(iid=revision_iid)
    )

    tests = (
        models.KCIDBTest.objects
        .filter(
            build__revision=revision
        )
        .select_related(
            'build',
            'test',
        )
        .prefetch_related(
            'issues',
        )
    )

    builds = (
        models.KCIDBBuild.objects
        .filter(
            revision=revision
        )
        .prefetch_related(
            'kcidbtest_set',
            'kcidbtest_set__test',
        )
    )

    issues = (
        models.Issue.objects
        .filter(
            resolved=False
        )
        .select_related(
            'kind'
        )
    )

    issue_occurrences = [
        {
            'issue': issue,
            'revisions': (
                issue.kcidbrevision_set.filter(iid=revision.iid)
            ),
            'builds': (
                issue.kcidbbuild_set
                .filter(revision=revision)
            ),
            'tests': (
                issue.kcidbtest_set
                .filter(build__revision=revision)
                .select_related(
                    'test',
                )
            ),
        } for issue in models.Issue.objects.filter(
            Q(kcidbrevision=revision) |
            Q(kcidbbuild__revision=revision) |
            Q(kcidbtest__build__revision=revision)
        ).select_related(
            'kind'
        ).distinct().order_by('-id')  # Ordering not respected by distinct.
    ]

    context = {
        'builds': builds,
        'builds_failed': builds.exclude(valid=True),
        'issues': issues,
        'issue_occurrences': issue_occurrences,
        'revision': revision,
        'revisions_failed': [revision] if not revision.valid else [],
        'tests': tests,
        'tests_failed': tests.exclude(status=models.ResultEnum.PASS),
    }

    return HttpResponse(template.render(context, request))


def builds_get(request, build_iid):
    """Get a single build."""
    template = loader.get_template('web/kcidb/build.html')

    tree = models.KCIDBBuild.objects.get(iid=build_iid).revision.tree
    if not tree.is_read_authorized(request):
        return HttpResponseNotFound()

    build = (
        models.KCIDBBuild.objects.aggregated()
        .select_related(
            'compiler',
            'log',
            'origin',
            'revision',
        )
        .prefetch_related(
            'input_files',
            'output_files',
            'kcidbtest_set',
            'kcidbtest_set__output_files',
            'kcidbtest_set__test',
        )
        .get(iid=build_iid)
    )

    tests = (
        models.KCIDBTest.objects
        .filter(
            build=build
        )
        .select_related(
            'build',
            'test',
        )
        .prefetch_related(
            'issues',
        )
    )

    issues = (
        models.Issue.objects
        .filter(
            resolved=False
        )
        .select_related(
            'kind'
        )
    )

    issue_occurrences = [
        {
            'issue': issue,
            'builds': (
                issue.kcidbbuild_set
                .filter(iid=build.iid)
            ),
            'tests': (
                issue.kcidbtest_set
                .filter(build=build)
                .select_related(
                    'test',
                )
            ),
        } for issue in models.Issue.objects.filter(
            Q(kcidbbuild=build) |
            Q(kcidbtest__build=build)
        ).select_related(
            'kind'
        ).distinct().order_by('-id')  # Ordering not respected by distinct.
    ]

    context = {
        'build': build,
        'builds_failed': [build] if not build.valid else [],
        'issues': issues,
        'issue_occurrences': issue_occurrences,
        'tests': tests,
        'tests_failed': tests.exclude(status=models.ResultEnum.PASS),
    }

    return HttpResponse(template.render(context, request))


def tests_get(request, test_iid):
    """Get a single test."""
    template = loader.get_template('web/kcidb/test.html')

    tree = models.KCIDBTest.objects.get(iid=test_iid).build.revision.tree
    if not tree.is_read_authorized(request):
        return HttpResponseNotFound()

    test = (
        models.KCIDBTest.objects
        .select_related(
            'build',
            'build__revision',
            'environment',
            'origin',
            'test',
        )
        .prefetch_related(
            'output_files',
        )
        .get(iid=test_iid)
    )

    issues = (
        models.Issue.objects
        .filter(
            resolved=False
        )
        .select_related(
            'kind'
        )
    )

    issue_occurrences = [
        {
            'tests': (
                issue.kcidbtest_set
                .filter(iid=test.iid)
            ),
            'issue': issue
        } for issue in models.Issue.objects.filter(
            kcidbtest=test
        ).select_related(
            'kind'
        ).distinct().order_by('-id')  # Ordering not respected by distinct.
    ]

    context = {
        'test': test,
        'tests_failed': [test] if not test.status or test.status != models.ResultEnum.PASS else [],
        'issues': issues,
        'issue_occurrences': issue_occurrences,
    }

    print(test.status, test.status == models.ResultEnum.PASS)

    return HttpResponse(template.render(context, request))


def kcidb_issue(request):
    """Link/Unlink revisions, builds or tests to a given issue."""
    if request.method == "POST":
        objects = {
            'revision': {
                'permission': 'datawarehouse.change_kcidbrevision',
                'elements': models.KCIDBRevision.objects.filter(
                    iid__in=request.POST.getlist('revision_iids')
                ),
            },
            'build': {
                'permission': 'datawarehouse.change_kcidbbuild',
                'elements': models.KCIDBBuild.objects.filter(
                    iid__in=request.POST.getlist('build_iids')
                ),
            },
            'test': {
                'permission': 'datawarehouse.change_kcidbtest',
                'elements': models.KCIDBTest.objects.filter(
                    iid__in=request.POST.getlist('test_iids')
                ),
            },
        }

        # Check all permissions before performing any change.
        for obj in objects.values():
            if obj['elements'] and not request.user.has_perm(obj['permission']):
                return HttpResponseForbidden()

            all_objects_authorized = authorization.PolicyAuthorizationBackend.all_objects_authorized(
                request,
                obj['elements'],
            )
            if not all_objects_authorized:
                return HttpResponseNotFound()

        issue_id = request.POST.get('issue_id')
        issue = models.Issue.objects.get(id=issue_id)

        action = request.POST.get('action', 'add')
        for obj in objects.values():
            for element in obj['elements']:
                if action == 'add':
                    element.issues.add(issue)
                elif action == 'remove':
                    element.issues.remove(issue)
                else:
                    HttpResponseBadRequest(f'Action {action} unknown.')

                LOGGER.info('action="%s issue on %s" user="%s" issue_id="%s" iid="%s"',
                            action, element.__class__.__name__, request.user.username, issue.id, element.iid)

    return HttpResponseRedirect(request.POST.get('redirect_to'))


def search(request):
    """Search for revisions by id or by pipeline_id."""
    template = loader.get_template('web/search.html')
    page = request.GET.get('page')
    query = request.GET.get('q')

    if not query:
        return HttpResponse(template.render({}, request))

    query_filter = (
        Q(id__icontains=query)
    )

    if query.isdigit():
        query_filter |= (
            Q(iid=query) |
            Q(gitlabjob__job_id=query) |
            Q(gitlabjob__pipeline__pipeline_id=query)
        )

    revisions = (
        models.KCIDBRevision.objects
        .filter_authorized(request)
        .filter(query_filter)
    )

    paginator = pagination.EndlessPaginator(
        revisions.values_list('iid', flat=True),
        30
    )
    revision_iids = paginator.get_page(page)

    revisions = (
        models.KCIDBRevision.objects
        .aggregated()
        .filter(iid__in=revision_iids)
        .prefetch_related(
            'tree',
            'gitlabjob_set',
            'gitlabjob_set__pipeline',
        )
    )

    context = {
        'revisions': revisions,
        'paginator': revision_iids,
        'query': query,
    }

    return HttpResponse(template.render(context, request))
