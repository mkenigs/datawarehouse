# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Urls file."""
from cki_lib import misc
import debug_toolbar
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include
from django.urls import path
from django.views.generic.base import RedirectView

from datawarehouse import views
from datawarehouse import views_kcidb

urlpatterns = [
    path('', RedirectView.as_view(url='kcidb/revisions', permanent=False)),
    path('status', views.status),
    path('dashboard', RedirectView.as_view(url='kcidb/revisions', permanent=False)),
    path('admin/', admin.site.urls),
    path('confidence/<str:group>', views.confidence, name='views.confidence'),
    path('cron', views.cron_run,),
    path('details/<str:group>/<int:item_id>', views.details,),
    path('issue/list/<str:group>', views.issue_list, name='issue_list'),
    path('issue/list', RedirectView.as_view(url='list/unresolved', permanent=False)),
    path('issue/regex', views.issue_regex_view, name='issue_regex'),
    path('issue/regex/<int:issue_regex_id>', views.issue_regex_get, name='views.issue_regex.get'),
    path('issue', views.issue_new_or_edit, name='views.issue.new_or_edit'),
    path('issue/<int:issue_id>', views.issue_get, name='views.issue.get'),
    path('issue/<int:issue_id>/resolve', views.issue_resolve,),
    path('search', views_kcidb.search, name='search'),

    path('kcidb/failures', RedirectView.as_view(url='failures/all', permanent=False)),
    path('kcidb/failures/<str:stage>',
         views_kcidb.revisions_list_by_failure, name='views.kcidb.failures'),
    path('kcidb/issues/occurrences', views_kcidb.kcidb_issue, name='views.kcidb.issues'),
    path('kcidb/revisions',
         views_kcidb.revisions_list, name='views.kcidb.revisions'),
    path('kcidb/revisions/<int:revision_iid>',
         views_kcidb.revisions_get, name='views.kcidb.revisions'),
    path('kcidb/builds/<int:build_iid>',
         views_kcidb.builds_get, name='views.kcidb.builds'),
    path('kcidb/tests/<int:test_iid>',
         views_kcidb.tests_get, name='views.kcidb.tests'),
    path('api/', include('datawarehouse.api.urls')),
    path('accounts/', include('datawarehouse.accounts.urls')),
    path('__debug__/', include(debug_toolbar.urls)),
] \
    + static(settings.MEDIA_PATH, document_root=settings.MEDIA_ROOT) \
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.FF_SAML_ENABLED:
    urlpatterns.append(
        path('saml2/', include('djangosaml2.urls'))
    )

# On non production environment, include /metrics urls.
if not misc.is_production():
    urlpatterns.append(
        path('prometheus/', include('django_prometheus.urls'))
    )
