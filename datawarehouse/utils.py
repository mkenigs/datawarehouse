# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Utils file."""
import datetime
import email

from celery import shared_task
from cki_lib.logger import get_logger
from cki_lib.timer import ScheduledTask
import dateutil
from django.conf import settings
from django.contrib.auth.models import User  # pylint: disable=imported-auth-user
from django.utils.timezone import make_aware
from requests_futures.sessions import FuturesSession
from rest_framework import generics

from datawarehouse import authorization
from datawarehouse import rabbitmq

LOGGER = get_logger(__name__)
if settings.RABBITMQ_CONFIGURED:
    MSG_QUEUE = rabbitmq.MessageQueue(
        settings.RABBITMQ_HOST,
        settings.RABBITMQ_PORT,
        settings.RABBITMQ_USER,
        settings.RABBITMQ_PASSWORD,
        settings.RABBITMQ_KEEPALIVE_S,
    )
    TIMER_MSG_QUEUE_SEND = ScheduledTask(
        settings.TIMER_MSG_QUEUE_SEND_PERIOD_S,
        MSG_QUEUE.send_thread
    )


def parse_patches_from_urls(patch_urls):
    """
    Get patch subject from list of patches' urls.

    The result respects the order of the input.
    """
    session = FuturesSession(max_workers=settings.REQUESTS_MAX_WORKERS)
    return [{
        'url': p,
        'subject': email.message_from_bytes(s.result().content)['Subject']
    } for p, s in [(p, session.get(p)) for p in patch_urls]]


def timestamp_to_datetime(timestamp):
    """Convert a timestamp string to a _timezone-aware_ datetime, while keeping None values."""
    try:
        time = datetime.datetime.fromisoformat(timestamp)
    except ValueError:
        time = dateutil.parser.parse(timestamp)
    except TypeError:
        return None

    try:
        return make_aware(time)
    except ValueError:  # tz info already set
        return time


def send_kcidb_notification(messages):
    """Send kcidb notifications over rabbitmq."""
    if not settings.RABBITMQ_CONFIGURED:
        LOGGER.debug("KCIDB notification skipped. No RabbitMQ server configured.")
        return

    messages = messages if isinstance(messages, list) else [messages]
    MSG_QUEUE.bulk_add(
        [
            (message, settings.RABBITMQ_EXCHANGE_KCIDB_NOTIFICATIONS)
            for message in messages
        ]
    )
    TIMER_MSG_QUEUE_SEND.start()


def clean_dict(data):
    """Remove keys with None value from dict."""
    return {
        key: value
        for key, value in data.items()
        if value is not None
    }


class MultipleFieldLookupMixin:
    # pylint: disable=too-few-public-methods
    """
    GET with multiple filters.

    Allows setting parameters as name:field to use a different value as the parameter
    key than the representation on the database.

    For example, revision_id:revision__origin_id takes a `revision_id` keyword parameter but
    filters the database with `revision__origin_id`.
    """

    lookup_fields = set()

    @staticmethod
    def parse_field(field, value):
        """Replace %%id wildcard with id or iid depending on the value."""
        if field.endswith('%%id'):
            if value.isdigit() and int(value) < 2e9:
                field = field.replace('%%id', 'iid')
            else:
                field = field.replace('%%id', 'id')

        return field

    def get_queryset(self):
        """Override get_queryset."""
        queryset = self.filter_queryset(self.queryset)

        filter_params = {}
        for param, field in self.lookup_fields:

            if param in self.kwargs.keys():
                # It's part of the url parameters
                value = self.kwargs[param]
            else:
                # It's part of the GET parameters
                value = self.request.GET.get(param)

            if not value:
                # It's missing, don't use it to filter.
                continue

            field = self.parse_field(field, value)
            filter_params[field] = value

        queryset = authorization.PolicyAuthorizationBackend.filter_authorized(
            self.request,
            queryset,
        )

        return queryset.filter(**filter_params)

    def get_object(self):
        """Override get_object."""
        queryset = self.get_queryset()
        return generics.get_object_or_404(queryset)


def filter_revisions_view(request, revisions):
    """
    Filter revisions depending on the GET parameters.

    From a list of pre defined filter parameters, try to filter the
    revisions.

    Returns the filtered revisions and the list of filters applied.
    """
    filters_list = {
        'filter_email': {'field': 'contacts__email__icontains'},
        'filter_gittrees': {'field': 'tree__name__in', 'is_list': True},
    }
    filters = {}

    for param in request.GET:
        filter_def = filters_list.get(param)
        if not filter_def:
            continue

        if filter_def.get('is_list', False):
            value = request.GET.getlist(param)
        else:
            value = request.GET.get(param)

        if not value:
            continue

        revisions = revisions.filter(**{filter_def['field']: value})
        filters[param] = value

    return revisions, filters


@shared_task
def async_send_email(user_id, subject, message):
    """Send email to user asyncronusly using celery shared task."""
    user = User.objects.get(id=user_id)
    user.email_user(
        subject=subject,
        message=message,
    )
