"""Context processors."""
from django.conf import settings


def settings_values(_):
    """Add values from the settings module into the templates context."""
    data = {
        'PRIVACY_POLICY_URL': settings.PRIVACY_POLICY_URL,
        'FF_SIGNUP_ENABLED': settings.FF_SIGNUP_ENABLED,
        'FF_SAML_ENABLED': settings.FF_SAML_ENABLED,
    }
    if settings.FF_SAML_ENABLED:
        data['SAML_NAME'] = settings.SAML_NAME

    return data
