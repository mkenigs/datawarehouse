# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2019 Red Hat, Inc.
"""Scripts file."""
from cki_lib.timer import ScheduledTask
from django.conf import settings

from .misc import LOGGER
from .misc import send_kcidb_object_for_retriage
from .misc import verify_issue_regression

TIMER_RETRIAGE = ScheduledTask(
    settings.TIMER_RETRIAGE_PERIOD_S,
    send_kcidb_object_for_retriage.delay)
