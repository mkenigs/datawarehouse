# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Misc scripts file."""
import datetime

from celery import shared_task
from cki_lib.logger import get_logger
from django.conf import settings
from django.template import loader
from django.utils import timezone

from datawarehouse import models
from datawarehouse import signals
from datawarehouse import utils

LOGGER = get_logger(__name__)


@shared_task
def send_kcidb_object_for_retriage(since_days_ago=14):
    """Add last n days objects to the queue for triaging."""
    date_from = timezone.now() - datetime.timedelta(days=since_days_ago)

    to_retriage = {
        'revision': models.KCIDBRevision.objects.filter(
            valid=False,
            discovery_time__gte=date_from,
        ),
        'build': models.KCIDBBuild.objects.filter(
            valid=False,
            start_time__gte=date_from
        ),
        'test': models.KCIDBTest.objects.filter(
            status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES,
            start_time__gte=date_from
        ),
    }

    for kind, objects in to_retriage.items():
        signals.kcidb_object.send(
            sender='scripts.misc.send_kcidb_object_for_retriage',
            status=models.ObjectStatusEnum.NEEDS_TRIAGE,
            object_type=kind,
            objects=objects,
        )


def notify_issue_regression(obj, issue):
    """Notify that an issue regression was detected."""
    email_template = loader.get_template('email_issue_regression.html')
    email_context = {
        'issue': issue,
        'obj': obj,
    }

    # Filter users with authorization to write the issue
    users = issue.users_write_authorized
    # Filter users with authorization to read the object
    users = [u for u in users if u in obj.users_read_authorized]
    # Filter users with permission to change issues
    users = [u for u in users if u.has_perm('datawarehouse.change_issue')]

    for user in users:
        utils.async_send_email.delay(
            user_id=user.id,
            subject=f'{issue.kind.tag} | Issue regression detected',
            message=email_template.render(email_context, {}),
        )


def verify_issue_regression(obj, issue):
    """Verify that a tagged issue is not a regression."""
    if not settings.FF_NOTIFY_ISSUE_REGRESSION:
        return

    if not issue.resolved:
        # Not resolved, not a regression.
        return

    if obj.issues.filter(id=issue.id).exists():
        # Already tagged, not a regression.
        return

    notify_issue_regression(obj, issue)
