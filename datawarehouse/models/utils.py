# pylint: disable=too-few-public-methods
"""Model utils."""

from django.db import models

from datawarehouse import authorization


class Manager(models.Manager):
    """Manager with authorization methods."""

    def filter_authorized(self, request):
        """Return authorized objects for the request."""
        return authorization.PolicyAuthorizationBackend.filter_authorized(
            request, self
        )


class GenericNameManager(Manager):
    """Natural key based on name."""

    def get_by_natural_key(self, name):
        """Lookup the object by the natural key."""
        return self.get(name=name)


class Model(models.Model):
    """Django Model with authorization methods."""

    def is_read_authorized(self, request):
        """Return True if the request is authorized to read this object."""
        return authorization.PolicyAuthorizationBackend.is_read_authorized(request, self)

    def is_write_authorized(self, request):
        """Return True if the request is authorized to write this object."""
        return authorization.PolicyAuthorizationBackend.is_write_authorized(request, self)

    @property
    def users_write_authorized(self):
        """Return list of users authorized to write this object."""
        return authorization.PolicyAuthorizationBackend.get_users_authorized(self, 'write')

    @property
    def users_read_authorized(self):
        """Return list of users authorized to read this object."""
        return authorization.PolicyAuthorizationBackend.get_users_authorized(self, 'read')

    class Meta:
        """Meta."""

        abstract = True
