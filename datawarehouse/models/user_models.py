"""Models related to the users."""
from django.contrib.auth.models import User  # pylint: disable=imported-auth-user
from django.db import models
from django_prometheus.models import ExportModelOperationsMixin as EMOM


class SAMLUser(EMOM('saml_user'), models.Model):
    """
    Model for SAMLUser.

    Linked to a regular Django Auth user represents that the
    user is linked to a SAML2 account.
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.user.username}'
