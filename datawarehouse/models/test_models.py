# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Models related to the test stage."""

from email.utils import parseaddr

from django.conf import settings
from django.db import models
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse.models import utils


class TestMaintainer(EMOM('test_maintainer'), models.Model):
    """Model for TestMaintainer."""

    name = models.CharField(max_length=50)
    email = models.EmailField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} <{self.email}>'

    @classmethod
    def create_from_address(cls, address):
        """Create Maintainer from address."""
        name, email = parseaddr(address)
        maintainer = cls.objects.update_or_create(
            email=email,
            defaults={'name': name},
        )[0]
        return maintainer


class TestManager(utils.Manager):
    """Natural key for Test."""

    def get_by_natural_key(self, name, fetch_url):
        """Lookup the object by the natural key."""
        return self.get(name=name, fetch_url=fetch_url)


class Test(EMOM('test'), utils.Model):
    """Model for Test."""

    name = models.CharField(max_length=200)
    fetch_url = models.URLField(blank=True, null=True)
    maintainers = models.ManyToManyField(TestMaintainer)
    universal_id = models.CharField(max_length=50, null=True)

    objects = TestManager()

    # This model does not have policies to limit the access.
    path_to_policy = None

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    def natural_key(self):
        """Return the natural key."""
        return (self.name, self.fetch_url)

    @property
    def confidence(self):
        """Confidence index."""
        result = (self.testrun_set
                  .annotate(passed_count=models.Count('id', filter=models.Q(passed=True)))
                  .aggregate(confidence=models.Avg('passed_count')))['confidence']
        return result if result is not None else 1

    @property
    def name_sanitized(self):
        """Turn a string into a path-compatible name."""
        return "".join(
            char if char.isalnum() else '_' for char in self.name
        )


class BeakerResourceManager(utils.Manager):
    """Natural key for BeakerResource."""

    def get_by_natural_key(self, fqdn):
        """Lookup the object by the natural key."""
        return self.get(fqdn=fqdn)


class BeakerResource(EMOM('beaker_resource'), utils.Model):
    """Model for BeakerResource."""

    fqdn = models.CharField(max_length=100, blank=True, null=True)

    path_to_policy = 'kcidbtest__build__revision__tree__policy'

    objects = BeakerResourceManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.fqdn}'

    def natural_key(self):
        """Return the natural key."""
        return (self.fqdn, )

    @property
    def confidence(self):
        """Confidence index."""
        result = (self.beakertestrun_set
                  .annotate(passed_count=models.Count('id', filter=models.Q(passed=True)))
                  .aggregate(confidence=models.Avg('passed_count')))['confidence']
        return result if result is not None else 1


class ResultEnum(models.TextChoices):
    """Enumeration of the possible test results."""

    ERROR = 'E', 'ERROR'
    FAIL = 'F', 'FAIL'
    PASS = 'P', 'PASS'
    DONE = 'D', 'DONE'
    SKIP = 'S', 'SKIP'
    NEW = 'N', 'NEW'


class BeakerTask(EMOM('beaker_task'), models.Model):
    """Model for BeakerTask."""

    task_id = models.IntegerField()
    recipe_id = models.IntegerField()

    kcidb_test = models.ForeignKey('KCIDBTest', on_delete=models.CASCADE, null=True)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.recipe_id} {self.task_id}'

    @property
    def recipe_url(self):
        """Return formatted recipe URL."""
        return f'{settings.BEAKER_URL}/recipes/{self.recipe_id}'

    @property
    def task_url(self):
        """Return formatted task URL."""
        return f'{self.recipe_url}#task{self.task_id}'

    @classmethod
    def create_from_misc(cls, test, misc):
        """Create BeakerTestRun from kcidb misc field."""
        testrun = cls.objects.get_or_create(
            task_id=misc['beaker']['task_id'],
            recipe_id=misc['beaker']['recipe_id'],
            kcidb_test=test,
        )[0]

        return testrun
