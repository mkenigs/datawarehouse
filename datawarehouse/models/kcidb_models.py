"""KCIDB schema models file."""
from email.utils import parseaddr

from cki_lib.misc import get_nested_key
from django.conf import settings
from django.db import models
from django.urls import reverse
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse import signals
from datawarehouse import utils
from datawarehouse.models import file_models
from datawarehouse.models import issue_models
from datawarehouse.models import patch_models
from datawarehouse.models import pipeline_models
from datawarehouse.models import test_models
from datawarehouse.models.utils import Manager
from datawarehouse.models.utils import Model


class ObjectStatusEnum(models.TextChoices):
    """Status of objects in messages."""

    NEW = 'new'
    NEEDS_TRIAGE = 'needs_triage'
    READY_TO_REPORT = 'ready_to_report'
    UPDATED = 'updated'


class MissingParent(Exception):
    """Object's parent is missing."""


def is_cki_submission(data):
    """Return True if this data matches a cki pipeline."""
    misc = data.get('misc', {})
    return {'pipeline', 'job'}.issubset(set(misc.keys()))


class Maintainer(EMOM('maintainer'), models.Model):
    """Model for Maintainer."""

    name = models.CharField(max_length=100)
    email = models.EmailField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} <{self.email}>'

    @classmethod
    def create_from_address(cls, address):
        """Create Maintainer from address."""
        name, email = parseaddr(address)
        maintainer = cls.objects.update_or_create(
            email=email,
            defaults={'name': name},
        )[0]
        return maintainer


class KCIDBOrigin(EMOM('kcidb_origin'), models.Model):
    """Model for KCIDBOrigin."""

    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        """Return __str__ formatted."""
        return self.name

    @classmethod
    def create_from_string(cls, name):
        """Create KCIDBOrigin from string."""
        return cls.objects.get_or_create(name=name)[0]


class KCIDBRevisionManager(Manager):
    # pylint: disable=too-few-public-methods
    """Manager for KCIDBRevision."""

    def aggregated(self):
        # pylint: disable=too-many-locals
        """Add aggregated information."""
        revision = KCIDBRevision.objects.filter(iid=models.OuterRef('iid'))
        revision_untriaged = revision.filter(
            valid=False,
            issues=None
        )

        builds_run = KCIDBBuild.objects.filter(revision__iid=models.OuterRef('iid'))
        builds_pass = builds_run.filter(valid=True)
        builds_fail = builds_run.filter(valid=False)
        builds_untriaged = builds_fail.filter(issues=None)

        tests_run = KCIDBTest.objects.filter(build__revision__iid=models.OuterRef('iid'))
        tests_pass = tests_run.filter(status=test_models.ResultEnum.PASS)
        tests_skip = tests_run.filter(status=test_models.ResultEnum.SKIP)
        tests_fail = tests_run.filter(status=test_models.ResultEnum.FAIL, waived=False)
        tests_untriaged = (
            KCIDBTest.objects
            .filter(
                build__revision__iid=models.OuterRef('iid'),
                status__in=KCIDBTest.UNSUCCESSFUL_STATUSES,
                issues=None
            )
        )
        tests_targeted = tests_run.filter(targeted=True)

        counts = {
            'builds_run': builds_run,
            'builds_pass': builds_pass,
            'builds_fail': builds_fail,
            'tests_run': tests_run,
            'tests_pass': tests_pass,
            'tests_skip': tests_skip,
            'tests_fail': tests_fail,
        }

        # Look for issues to determine if there are objects triaged.
        revision_issues = issue_models.Issue.objects.filter(
            kcidbrevision__iid=models.OuterRef('iid')
        )
        build_issues = issue_models.Issue.objects.filter(
            kcidbbuild__revision__iid=models.OuterRef('iid')
        )
        test_issues = issue_models.Issue.objects.filter(
            kcidbtest__build__revision__iid=models.OuterRef('iid')
        )

        annotations = {
            # Child objects passed
            'stats_builds_passed': ~models.Exists(builds_fail),
            'stats_tests_passed': ~models.Exists(tests_fail),

            # Objects have issues
            'stats_revision_triaged': models.Exists(revision_issues),
            'stats_builds_triaged': models.Exists(build_issues),
            'stats_tests_triaged': models.Exists(test_issues),

            # Objects have failures without issues
            'stats_revision_untriaged': models.Exists(revision_untriaged),
            'stats_builds_untriaged': models.Exists(builds_untriaged),
            'stats_tests_untriaged': models.Exists(tests_untriaged),

            # Revision has targeted tests
            'stats_tests_targeted': models.Exists(tests_targeted),
        }

        # Add count element for all the queries listed on $counts.
        for name, query in counts.items():
            count = query.values('iid').annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)


class KCIDBRevision(EMOM('kcidb_revision'), Model):
    """Model for KCIDBRevision."""

    iid = models.AutoField(primary_key=True)

    id = models.CharField(max_length=300, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    tree = models.ForeignKey('GitTree', on_delete=models.PROTECT, null=True, blank=True)
    git_repository_url = models.URLField(max_length=400, null=True, blank=True)
    git_repository_branch = models.CharField(max_length=200, null=True, blank=True)
    git_commit_hash = models.CharField(max_length=40, null=True, blank=True)
    git_commit_name = models.CharField(max_length=100, null=True, blank=True)
    patches = models.ManyToManyField('Patch', related_name='revisions', blank=True)
    message_id = models.CharField(null=True, blank=True, max_length=200)
    description = models.TextField(null=True, blank=True)
    publishing_time = models.DateTimeField(null=True, blank=True)
    discovery_time = models.DateTimeField(null=True, blank=True)
    contacts = models.ManyToManyField('Maintainer', related_name='revisions', blank=True)
    log = models.ForeignKey('Artifact', on_delete=models.SET_NULL, null=True, blank=True)
    valid = models.BooleanField(null=True)

    # Fields not belonging to KCIDB schema
    kernel_version = models.CharField(max_length=100, blank=True, null=True)
    issues = models.ManyToManyField('Issue')

    objects = KCIDBRevisionManager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'tree__policy'

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    class Meta:
        """Metadata."""

        ordering = ('-iid',)

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.kcidb.revisions', args=[self.iid])

    @property
    def is_public(self):
        """
        Return True if this KCIDBRevision is public.

        If there's no MergeRun associated, or any of the MergeRuns
        associated with this KCIDBRevision has a kernel_type
        different to 'upstream', consider it private.
        """
        return (
            self.gitlabjob_set.exists() and
            not self.gitlabjob_set.exclude(
                pipeline__variables__key='kernel_type',
                pipeline__variables__value='upstream'
            ).exists()
        )

    @property
    def is_triaged(self):
        """Return True if this revision is triaged."""
        if hasattr(self, 'stats_revision_triaged'):
            return self.stats_revision_triaged  # pylint: disable=no-member

        return self.issues.exists()

    @property
    def is_missing_triage(self):
        """Return True if this revision is missing triage."""
        if hasattr(self, 'stats_revision_untriaged'):
            return self.stats_revision_untriaged  # pylint: disable=no-member

        return not self.valid and not self.issues.exists()

    @property
    def builds_triaged(self):
        """Return list of triaged builds."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_builds_triaged') and not self.stats_builds_triaged:
            return KCIDBBuild.objects.none()

        return self.kcidbbuild_set.exclude(issues=None)

    @property
    def builds_untriaged(self):
        """Return list of untriaged builds."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_builds_untriaged') and not self.stats_builds_untriaged:
            return KCIDBBuild.objects.none()

        return self.kcidbbuild_set.filter_untriaged()

    @property
    def tests_triaged(self):
        """Return list of triaged tests."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_tests_triaged') and not self.stats_tests_triaged:
            return KCIDBTest.objects.none()

        return (
            KCIDBTest.objects
            .filter(build__revision=self)
            .exclude(issues=None)
        )

    @property
    def tests_untriaged(self):
        """Return list of untriaged tests."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_tests_untriaged') and not self.stats_tests_untriaged:
            return KCIDBTest.objects.none()

        return (
            KCIDBTest.objects
            .filter_untriaged()
            .filter(build__revision=self)
        )

    @property
    def has_objects_missing_triage(self):
        """Return if there are failures missing triage."""
        try:
            return (
                self.stats_revision_untriaged or
                self.stats_builds_untriaged or
                self.stats_tests_untriaged
            )
        except AttributeError:
            return None

    @property
    def has_objects_with_issues(self):
        """Return if the revision or one of it's builds and tests has any issues."""
        try:
            return (
                self.stats_revision_triaged or
                self.stats_builds_triaged or
                self.stats_tests_triaged
            )
        except AttributeError:
            return None

    @property
    def has_targeted_tests(self):
        """Return True if the revision has targeted tests."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_tests_targeted'):
            return self.stats_tests_targeted

        return (
            KCIDBTest.objects
            .filter(build__revision=self, targeted=True)
            .exists()
        )

    @classmethod
    def create_from_json(cls, data):
        """Create KCIDBRevision from kcidb json."""
        misc = data.get('misc', {})

        log_url = data.get('log_url')
        log = file_models.Artifact.create_from_url(log_url) if log_url else None

        tree_name = data.get('tree_name')
        tree = pipeline_models.GitTree.create_from_string(tree_name) if tree_name else None

        origin = KCIDBOrigin.create_from_string(data['origin'])

        revision, created = cls.objects.update_or_create(
            id=data['id'],
            origin=origin,
            defaults=utils.clean_dict({
                'tree': tree,
                'git_repository_url': data.get('git_repository_url'),
                'git_repository_branch': data.get('git_repository_branch'),
                'git_commit_hash': data.get('git_commit_hash'),
                'git_commit_name': data.get('git_commit_name'),
                'message_id': data.get('message_id'),
                'description': data.get('description'),
                'publishing_time': data.get('publishing_time'),
                'discovery_time': data.get('discovery_time'),
                'valid': data.get('valid'),
                'log': log,
                'kernel_version': get_nested_key(misc, 'job/kernel_version'),
            })
        )

        for contact in data.get('contacts', []):
            maintainer = Maintainer.create_from_address(contact)
            revision.contacts.add(maintainer)

        # Create the patch instances depending on the cki_pipeline_type.
        patches_data = data.get('patch_mboxes', [])
        if patches_data:
            patch_urls = [patch['url'] for patch in patches_data]
            patches = patch_models.Patch.create_from_urls(patch_urls)
            revision.patches.set(patches)

        if is_cki_submission(data):
            pipeline_models.GitlabJob.create_from_misc(misc, kcidb_revision=revision)

        signals.kcidb_object.send(
            sender='kcidb.revision.create_from_json',
            status=ObjectStatusEnum.NEW if created else ObjectStatusEnum.UPDATED,
            object_type='revision',
            objects=[revision],
        )

        return revision


class KCIDBBuildManager(Manager):
    # pylint: disable=too-few-public-methods
    """Manager for KCIDBBuild."""

    def aggregated(self):
        """Add aggregated information."""
        tests_run = KCIDBTest.objects.filter(build__id=models.OuterRef('id'))
        tests_pass = tests_run.filter(status=test_models.ResultEnum.PASS)
        tests_skip = tests_run.filter(status=test_models.ResultEnum.SKIP)
        tests_fail = tests_run.filter(status=test_models.ResultEnum.FAIL, waived=False)
        tests_targeted = tests_run.filter(targeted=True)

        counts = {
            'tests_run': tests_run,
            'tests_pass': tests_pass,
            'tests_skip': tests_skip,
            'tests_fail': tests_fail,
        }

        annotations = {
            'stats_tests_passed': ~models.Exists(tests_fail),
            'stats_tests_targeted': models.Exists(tests_targeted),
        }

        # Add count element for all the queries listed on $counts.
        for name, query in counts.items():
            count = query.values('id').annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def filter_untriaged(self):
        """Filter untriaged builds."""
        return self.filter(
            valid=False,
            issues=None
        )


class KCIDBBuild(EMOM('kcidb_build'), Model):
    """Model for KCIDBBuild."""

    iid = models.AutoField(primary_key=True)

    revision = models.ForeignKey('KCIDBRevision', on_delete=models.CASCADE)
    id = models.CharField(max_length=200, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    description = models.TextField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    command = models.TextField(null=True, blank=True)
    compiler = models.ForeignKey('Compiler', on_delete=models.PROTECT, null=True, blank=True)
    input_files = models.ManyToManyField('Artifact', related_name='build_input', blank=True)
    output_files = models.ManyToManyField('Artifact', related_name='build_output', blank=True)
    config_name = models.CharField(max_length=20, null=True, blank=True)
    config_url = models.URLField(max_length=400, null=True, blank=True)
    log = models.ForeignKey('Artifact', on_delete=models.SET_NULL, null=True, blank=True)
    valid = models.BooleanField(null=True)

    architecture = models.IntegerField(choices=pipeline_models.ArchitectureEnum.choices,
                                       null=True, blank=True)

    issues = models.ManyToManyField('Issue')

    objects = KCIDBBuildManager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'revision__tree__policy'

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    class Meta:
        """Metadata."""

        ordering = ('-iid',)

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.kcidb.builds', args=[self.iid])

    @property
    def is_public(self):
        """
        Return True if this KCIDBBuild is public.

        If there's no BuildRun associated, or any of the BuildRuns
        associated with this KCIDBBuild has a kernel_type
        different to 'upstream', consider it private.
        """
        return (
            self.gitlabjob_set.exists() and
            not self.gitlabjob_set.exclude(
                pipeline__variables__key='kernel_type',
                pipeline__variables__value='upstream'
            ).exists()
        )

    @property
    def tests_passed(self):
        """Return True if all tests passed."""
        try:
            return not bool(self.stats_tests_fail_count)
        except AttributeError:
            return not self.kcidbtest_set.filter(status=test_models.ResultEnum.FAIL, waived=False).exists()

    @property
    def has_targeted_tests(self):
        """Return True if the build has targeted tests."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_tests_targeted'):
            return self.stats_tests_targeted

        return (
            KCIDBTest.objects
            .filter(build=self, targeted=True)
            .exists()
        )

    @property
    def all_passed(self):
        """Return True if the build and tests passed."""
        return self.valid and self.tests_passed

    @classmethod
    def create_from_json(cls, data):
        """Create KCIDBBuild from kcidb json."""
        misc = data.get('misc', {})

        arch = pipeline_models.ArchitectureEnum[data['architecture']] if data.get('architecture') else None

        compiler = data.get('compiler')
        compiler = pipeline_models.Compiler.objects.get_or_create(name=compiler)[0] if compiler else None

        log = data.get('log_url')
        log = file_models.Artifact.create_from_url(log) if log else None

        try:
            revision = KCIDBRevision.objects.get(id=data['revision_id'], origin__name=data['origin'])
        except KCIDBRevision.DoesNotExist as exc:
            raise MissingParent('Parent Revision is not present on the DB') from exc

        origin = KCIDBOrigin.create_from_string(data['origin'])
        build, created = cls.objects.update_or_create(
            revision=revision,
            id=data['id'],
            origin=origin,
            defaults=utils.clean_dict({
                'description': data.get('description'),
                'start_time': data.get('start_time'),
                'duration': data.get('duration'),
                'architecture': arch,
                'command': data.get('command'),
                'compiler': compiler,
                'config_name': data.get('config_name'),
                'config_url': data.get('config_url'),
                'log': log,
                'valid': data.get('valid'),
            })
        )

        for input_file in data.get('input_files', []):
            file = file_models.Artifact.objects.create(
                name=input_file['name'],
                url=input_file['url'],
            )
            build.input_files.add(file)

        for output_file in data.get('output_files', []):
            file = file_models.Artifact.objects.create(
                name=output_file['name'],
                url=output_file['url'],
            )
            build.output_files.add(file)

        if is_cki_submission(data):
            pipeline_models.GitlabJob.create_from_misc(misc, kcidb_build=build)

        signals.kcidb_object.send(
            sender='kcidb.build.create_from_json',
            status=ObjectStatusEnum.NEW if created else ObjectStatusEnum.UPDATED,
            object_type='build',
            objects=[build],
        )

        return build


class KCIDBTestManager(Manager):
    # pylint: disable=too-few-public-methods
    """Manager for KCIDBTestManager."""

    def filter_untriaged(self):
        """Filter untriaged tests."""
        return self.filter(
            status__in=KCIDBTest.UNSUCCESSFUL_STATUSES,
            issues=None
        )


class KCIDBTest(EMOM('kcidb_test'), Model):
    """Model for KCIDBTest."""

    iid = models.AutoField(primary_key=True)

    build = models.ForeignKey('KCIDBBuild', on_delete=models.CASCADE)
    id = models.CharField(max_length=200, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    environment = models.ForeignKey('BeakerResource', on_delete=models.PROTECT, null=True, blank=True)
    test = models.ForeignKey('Test', on_delete=models.PROTECT, null=True, blank=True)
    waived = models.BooleanField(null=True)
    start_time = models.DateTimeField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    output_files = models.ManyToManyField('Artifact', related_name='test', blank=True)

    status = models.CharField(max_length=1, choices=test_models.ResultEnum.choices,
                              null=True, blank=True)

    # Fields not belonging to KCIDB schema
    kernel_debug = models.BooleanField(null=True, blank=True)
    targeted = models.BooleanField(null=True, blank=True)
    issues = models.ManyToManyField('Issue')

    objects = KCIDBTestManager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'build__revision__tree__policy'

    UNSUCCESSFUL_STATUSES = (test_models.ResultEnum.ERROR, test_models.ResultEnum.FAIL)

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    class Meta:
        """Metadata."""

        ordering = ('iid',)

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.kcidb.tests', args=[self.iid])

    @property
    def is_public(self):
        """
        Return True if this KCIDBTest is public.

        If there's no GitJob associated, or any of the GitJobs
        associated with this KCIDBTest has a kernel_type
        different to 'upstream', consider it private.
        """
        return (
            self.gitlabjob_set.exists() and
            not self.gitlabjob_set.exclude(
                pipeline__variables__key='kernel_type',
                pipeline__variables__value='upstream'
            ).exists()
        )

    @classmethod
    def create_from_json(cls, data):
        """Create KCIDBTest from kcidb json."""
        misc = data.get('misc', {})

        try:
            build = KCIDBBuild.objects.get(id=data['build_id'])
        except KCIDBBuild.DoesNotExist as exc:
            raise MissingParent('Parent Build is not present on the DB') from exc

        environment = data.get('environment')
        environment = test_models.BeakerResource.objects.get_or_create(
            fqdn=environment['description']
        )[0] if environment else None

        test_path = data.get('path')
        test_description = data.get('description')
        if test_description:
            test = test_models.Test.objects.update_or_create(
                name=test_description,
                defaults={
                    'universal_id': test_path,
                }
            )[0]

            maintainers = misc.get('maintainers') or []
            if isinstance(maintainers, str):
                maintainers = maintainers.split(',')

            test.maintainers.set(
                [
                    test_models.TestMaintainer.create_from_address(address)
                    for address in maintainers
                ]
            )
        else:
            test = None

        status = test_models.ResultEnum[data['status']] if data.get('status') else None

        origin = KCIDBOrigin.create_from_string(data['origin'])
        test, created = cls.objects.update_or_create(
            build=build,
            id=data['id'],
            origin=origin,
            defaults=utils.clean_dict({
                'environment': environment,
                'test': test,
                'status': status,
                'waived': data.get('waived'),
                'start_time': data.get('start_time'),
                'duration': data.get('duration'),
                'kernel_debug': misc.get('debug', False),
                'targeted': misc.get('targeted', False),
            })
        )

        for output_file in data.get('output_files', []):
            file = file_models.Artifact.objects.create(
                name=output_file['name'],
                url=output_file['url'],
            )
            test.output_files.add(file)

        if is_cki_submission(data):
            pipeline_models.GitlabJob.create_from_misc(misc, kcidb_test=test)
            if 'beaker' in misc.keys():
                test_models.BeakerTask.create_from_misc(test, misc)

        signals.kcidb_object.send(
            sender='kcidb.test.create_from_json',
            status=ObjectStatusEnum.NEW if created else ObjectStatusEnum.UPDATED,
            object_type='test',
            objects=[test],
        )

        return test
