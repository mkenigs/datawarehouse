"""Authorization framework models."""
import django.contrib.auth.models as auth_models
from django.db import models
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse.models import utils


class Policy(EMOM('policy'), models.Model):
    """
    Model for policy.

    Represents the rules defined to access an object.

    Users requiring to read or write the object associated to this policy need
    to be part of the read_group or write_group respectively.
    """

    name = models.CharField(max_length=40, unique=True)
    read_group = models.ForeignKey(auth_models.Group, on_delete=models.PROTECT,
                                   related_name='read_policies',
                                   null=True, blank=True)
    write_group = models.ForeignKey(auth_models.Group, on_delete=models.PROTECT,
                                    related_name='write_policies',
                                    null=True, blank=True)

    objects = utils.GenericNameManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} (read: {self.read_group}, write: {self.write_group})'
