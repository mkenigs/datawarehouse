#!/bin/bash

set -euo pipefail

DATAWAREHOUSE_IMAGE=registry.gitlab.com/cki-project/datawarehouse/datawarehouse
DB_IMAGE=docker.io/library/postgres:11-alpine

function stop(){
    echo -e "\n>> Stoping and cleaning previous datawarehouse pod"
    podman pod rm -f datawarehouse 2> /dev/null
    podman volume rm -f datawarehouse-db-data 2> /dev/null
}

function start() {
    if podman pod exists datawarehouse ; then
        echo -e "\n>> Start previous datawarehouse pod"
        podman pod start datawarehouse
        exit 0
    fi

    echo -e "\n>> Creating datawarehouse pod"
    podman pod create -n datawarehouse -p 5432:5432 -p 8000:8000

    echo -e "\n>> Creating datawarehouse-db container"
    podman run -d \
        --pod datawarehouse \
        --name datawarehouse-db \
        --expose 5432 \
        -e POSTGRES_PASSWORD="${POSTGRES_PASSWORD:-password}" \
        -e POSTGRES_USER="${POSTGRES_USER:-datawarehouse}" \
        -e POSTGRES_NAME="${POSTGRES_NAME:-datawarehouse}" \
        --volume ./db:/docker-entrypoint-initdb.d:z \
        --pull always \
        $DB_IMAGE

    sleep 3

    echo -e "\n>> Creating datawarehouse-web container"
    podman run -d \
        --pod datawarehouse \
        --name datawarehouse-web \
        --expose 8000 \
        -e DB_HOST="${DB_HOST:-localhost}" \
        -e DB_PASSWORD="${POSTGRES_PASSWORD:-password}" \
        -e GITLAB_URL="${GITLAB_URL:-https://gitlab.url}" \
        -e BEAKER_URL="${BEAKER_URL:-https://beaker.url}" \
        -e SECRET_KEY="${SECRET_KEY:-secret-key}" \
        -e RABBITMQ_HOST="${RABBITMQ_HOST:-rabbitmq}" \
        -e RABBITMQ_PORT="${RABBITMQ_PORT:-5672}" \
        --ulimit nofile=90000:90000 \
        --security-opt label=disable \
        -v .:/code \
        --pull always \
        $DATAWAREHOUSE_IMAGE \
        sh -c "
            pip install tox pytest pytest-django
            dnf install -y --repo=fedora jq lolcat toilet nmap-ncat
            until nc -z localhost 5432; do sleep 3; done
            /code/entrypoint.sh
        "

    cat <<EOF
>>
>> Datawarehouse started, but it could take a minute to be ready
>> To check if it's ready you can check the logs:
>>
>>  podman logs -f datawarehouse-web
>>
>> Then you can access to the Datawarehouse app at: http://0.0.0.0:8000
>> You can run the tests with the following command:
>>
>>  podman exec -it datawarehouse-web tox
>>
EOF
}

function restart() {
    local service="$1"
    if [ "$service" == "all" ]; then
        echo -e "\n>> Restarting the datawarehouse pod"
        podman pod restart datawarehouse
    else
        echo -e "\n>> Restarting the service datawarehouse-${service}"
        podman restart "datawarehouse-${service}"
    fi
}

function reset() {
    stop
    start
}

function usage() {
    program="$0"
    cat <<EOF
Usage:
    $program [up] [down] [restart [web|db|all] ] [reset]
    $program -h|--help|help

Examples:
    $program up
    $program down
    $program restart web
    $program restart all
    $program reset

Options:
    up          Create and start containers
    down        Stop and remove containers and the pod
    restart     Restart the services
    reset       Remove old containers and start a new ones
    help        Show this help

EOF
}

while [ "$#" -gt 0 ]
do
    case $1 in
    up)
        start
        exit 0
        ;;
    down)
        stop
        exit 0
        ;;
    restart)
        shift 1
        service=$1
        restart "$service"
        exit 0
        ;;
    reset)
        reset
        exit 0
        ;;
    -h|--help|help)
        usage
        exit 0
        ;;
    *)
        usage
        exit 0
        ;;
    esac
done

# In case no option is passed
usage
