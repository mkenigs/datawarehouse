# KCIDB - Build

## Get

Get a single build.

`GET /api/1/kcidb/builds/$build_id`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `build_id` | `str/int` | Yes | `id` or `iid` of the build to get. |

Example of response:

```json
{
    "revision_id": "b8fba93561c984f336d47d6d544be3a2a920bac3",
    "id": "redhat:926214",
    "origin": "redhat",
    "start_time": "2020-06-30T17:10:20.113000Z",
    "duration": 415,
    "architecture": "x86_64",
    "command": "make -j30 INSTALL_MOD_STRIP=1 targz-pkg",
    "compiler": "gcc (GCC) 10.1.1 20200507 (Red Hat 10.1.1-1)",
    "output_files": [
        {
            "url": "https://s3.url/datawarehouse/609265/build_x86_64/kernel-stable-queue-x86_64-b8fba93561c984f336d47d6d544be3a2a920bac3.tar.gz",
            "name": "kernel-stable-queue-x86_64-b8fba93561c984f336d47d6d544be3a2a920bac3.tar.gz"
        }
    ],
    "config_name": "fedora",
    "config_url": "https://s3.url/datawarehouse/609265/build_x86_64/.config",
    "log_url": "https://s3.url/datawarehouse/609265/build_x86_64/build.log",
    "valid": true,
    "misc": {
        "iid": 390
    }
}
```

## List

Get a list of builds for a revisions.

`GET /api/1/kcidb/revisions/$revision_id/builds`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `revision_id` | `str/int` | Yes | `id` or `iid` of the build's revision. |

Example of response:

```json
{
    "count": 4,
    "next": null,
    "previous": null,
    "results": [
        {
            "revision_id": "b8fba93561c984f336d47d6d544be3a2a920bac3",
            "id": "redhat:926214",
            "origin": "redhat",
            "start_time": "2020-06-30T17:10:20.113000Z",
            "duration": 415,
            "architecture": "x86_64",
            "command": "make -j30 INSTALL_MOD_STRIP=1 targz-pkg",
            "compiler": "gcc (GCC) 10.1.1 20200507 (Red Hat 10.1.1-1)",
            "output_files": [
                {
                    "url": "https://s3.url/datawarehouse/609265/build_x86_64/kernel-stable-queue-x86_64-b8fba93561c984f336d47d6d544be3a2a920bac3.tar.gz",
                    "name": "kernel-stable-queue-x86_64-b8fba93561c984f336d47d6d544be3a2a920bac3.tar.gz"
                }
            ],
            "config_name": "fedora",
            "config_url": "https://s3.url/datawarehouse/609265/build_x86_64/.config",
            "log_url": "https://s3.url/datawarehouse/609265/build_x86_64/build.log",
            "valid": true,
            "misc": {
                "iid": 390
            }
        },
        ...
    ]
}
```
