# Issue

To learn about issues, check the [Issues page].

## Get

Get a single Issue.

`GET /api/1/issue/$issue_id`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `issue_id` | `int` | Yes | ID of the Issue. |

Example of response:

```json
{
    "id": 1,
    "kind": {
        "id": 1,
        "description": "Kernel bug",
        "tag": "Kernel Bug"
    },
    "description": "Bug description.",
    "ticket_url": "https://bugzilla.redhat.com/show_bug.cgi?id=123456"
    "resolved": false,
    "generic": false
},
```

## List

Get a list of Issues.

`GET /api/1/issue`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `resolved` | `bool` | No | Filter resolved or not resolved issues. |

Example of response:

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "kind": {
                "id": 1,
                "description": "Kernel bug",
                "tag": "Kernel Bug"
            },
            "description": "Bug description.",
            "ticket_url": "https://bugzilla.redhat.com/show_bug.cgi?id=123456"
            "resolved": false,
            "generic": false
        }
    ]
}
```

[Issues Page]: ../issues.md
